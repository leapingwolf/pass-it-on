## Pass it on ##
Pass it on is created to share stuff among fellow citizens.
List your couch, bag of toys or rollerskates, set a date to claim by, and get connected with someone who needs it.
The app lets you search most recently added listings, check their availability and location and look up pictures.
Also, you could list something you need and let potential donors contact you though the messaging system to arrange pickups or make some enquiries.

![New.png](https://bitbucket.org/repo/EB6BLk/images/4005648002-New.png)



![Messages.png](https://bitbucket.org/repo/EB6BLk/images/1487145131-Messages.png)

### Prerequisite ###
* Initially set up the server to connect to the api server for the project.
Server installation documentation and repository is at [Aerialroots](https://bitbucket.org/leapingwolf/aerialroots)

### Set up ###
* You can use Android Studio or Intellij to work with this repository.
Three plugins for Android Studio are also required: Kotlin, Kotlin Extensions for Android and [io.mironov.smuggler](https://github.com/nsk-mironov/smuggler)

* Set up google maps api for the project [on Google Developer Console](https://console.developers.google.com)

* Replace <Your google maps api key here> in /app/src/main/res/values/google_maps_api.xml file with the key

* Import the project in  Android Studio or Intellij.


```sh
  $ git clone git@bitbucket.org:leapingwolf/pass-it-on.git
```

* Compile, build and run the project