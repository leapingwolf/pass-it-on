package de.com.pass_it_on.utils

/**
 * Created by akshata on 23/06/16.
 */
interface FragmentBackPressedListener {
    fun doBack()
}
