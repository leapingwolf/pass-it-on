package de.com.pass_it_on.utils;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import pl.tajchert.nammu.Nammu;

/**
 * Created by akshata on 09/09/16.
 */
public class GoogleApiHelper implements GoogleApiClient.ConnectionCallbacks{
    private static final String TAG = GoogleApiHelper.class.getSimpleName();

    GoogleApiClient mGoogleApiClient;
    Context mContext;
    Location mLocation;

    public GoogleApiHelper(Context context) {
        mContext = context;
        buildGoogleApiClient();
        connect();
    }

    public GoogleApiClient getGoogleApiClient() {
        return this.mGoogleApiClient;
    }

    public void connect() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    public void disconnect() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public boolean isConnected() {
        if (mGoogleApiClient != null) {
            return mGoogleApiClient.isConnected();
        } else {
            return false;
        }
    }
    
    public  Location getCurrentLocation(){
        Boolean permissionCheck = Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        if(permissionCheck) mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if(mLocation !=null) {
            Log.d("ApiConnectionHelper", String.valueOf(mLocation.getLatitude()));
        }
        else{
            Log.d("ApiConnectionHelper","location is null");
        }
        return mLocation;
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();

//                .addOnConnectionFailedListener(this)
//                .addApi(LocationServices.API).build();

    }

    @Override
    public void onConnected(Bundle bundle) {
        //You are connected do what ever you want
        //Like i get last known location
//        Boolean permissionCheck = Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION);
//        if(permissionCheck) mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//        mLocation = null;
        Log.d( "ApiConnectionHelper",  "connected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

}
