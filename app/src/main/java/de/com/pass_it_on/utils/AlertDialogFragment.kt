package de.com.pass_it_on.utils

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import de.com.pass_it_on.R
import de.com.pass_it_on.network.HttpHandler


/**
 * Created by akshata on 23/06/16.
 */
class AlertDialogFragment : DialogFragment() {
    private var mMessage: String? = null
    private var mResultCode: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mMessage = arguments.getString("message")
            mResultCode = arguments.getInt("resultCode")
        }
    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val dlgAlert = AlertDialog.Builder(activity);
            dlgAlert.setMessage(mMessage)
            dlgAlert.setTitle(resources.getText(R.string.app_name));
            dlgAlert.setPositiveButton("OK", DialogInterface.OnClickListener { dialogInterface, i ->
                dialogInterface.dismiss()
                activity.setResult(mResultCode)
                activity.finish()
            })
            dlgAlert.setNegativeButton("CANCEL", DialogInterface.OnClickListener { dialogInterface, i ->
                //do nothing
            })
            dlgAlert.setCancelable(true);
            return dlgAlert.create()
        }

    companion object {
        fun newInstance(bundle: Bundle?): AlertDialogFragment {
            val fragment = AlertDialogFragment()
            val args = bundle
            fragment.arguments = args
            return fragment
        }

    }

}

