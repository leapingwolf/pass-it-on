package de.com.pass_it_on.utils

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.View
import android.widget.DatePicker
import android.widget.EditText
import java.util.*

/**
 * Created by akshata on 23/06/16.
 */

class DatePickerDialogFragment(private val editText: EditText, private val ctx: Context) : DialogFragment(), DatePickerDialog.OnDateSetListener, View.OnFocusChangeListener {
    private var calendar: Calendar? = null

    init {
        this.editText.setOnFocusChangeListener(this)
        calendar = Calendar.getInstance()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val calendar = Calendar.getInstance()
        val yy = calendar.get(Calendar.YEAR)
        val mm = calendar.get(Calendar.MONTH)
        val dd = calendar.get(Calendar.DAY_OF_MONTH)
        return DatePickerDialog(activity, this, yy, mm, dd)
    }

    override fun onDateSet(view: DatePicker, yy: Int, mm: Int, dd: Int) {
        populateSetDate(yy, mm + 1, dd)
    }

    fun populateSetDate(year: Int, month: Int, day: Int) {
        try {
            this.editText!!.setText(day.toString() + "/" + month + "/" + year);
        } catch(ex: Exception) {
            Log.d("DatePickerDialog", "set text error" + ex.message.toString())
        }
    }

    override fun onFocusChange(v: View, hasFocus: Boolean) {
        // TODO Auto-generated method stub
        if (hasFocus) {
            DatePickerDialog(ctx, this, calendar!!.get(Calendar.YEAR), calendar!!.get(Calendar.MONTH),
                    calendar!!.get(Calendar.DAY_OF_MONTH)).show()
        }
    }

}