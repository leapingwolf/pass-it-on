package de.com.pass_it_on.utils;

import android.content.Context;
import android.support.v7.widget.SearchView;
import android.util.Log;

/**
 * Created by akshata on 01/06/16.
 */
public class ExtendedSearchView extends SearchView {

        OnSearchViewCollapsedEventListener mSearchViewCollapsedEventListener;
        OnQueryTextListener mQueryTextListener;

        public ExtendedSearchView(Context context) {
            super(context);
        }

        @Override
        public void onActionViewCollapsed() {
            if (mSearchViewCollapsedEventListener != null)
                mSearchViewCollapsedEventListener.onSearchViewCollapsed();
            super.onActionViewCollapsed();
            Log.d("AR", "view collapsed");
        }

        public interface OnSearchViewCollapsedEventListener{
            public void onSearchViewCollapsed();
        }

        public void setOnSearchViewCollapsedEventListener(OnSearchViewCollapsedEventListener eventListener) {
            mSearchViewCollapsedEventListener = eventListener;
        }
    }
