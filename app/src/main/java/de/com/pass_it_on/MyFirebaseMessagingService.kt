/**
 * Copyright 2016 Google Inc. All Rights Reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.com.pass_it_on

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.GsonBuilder
import de.com.pass_it_on.activities.LoginActivity
import de.com.pass_it_on.models.ItemModel
import de.com.pass_it_on.models.UserModel
import de.com.pass_it_on.network.SessionManager

class MyFirebaseMessagingService : FirebaseMessagingService() {

    /**
     * Called when message is received.

     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        // Not getting messages here? See why this may be: https://xgoo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage!!.from)

        // Check if message contains a data payload.
        if (remoteMessage.data.size > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)

            // Check if message contains a notification payload.
                var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
                val item = gson.fromJson(remoteMessage.data["item"], ItemModel::class.java)
                val user = gson.fromJson(remoteMessage.data["user"], UserModel::class.java)
                sendNotification(item, user, remoteMessage.data["click_action"], remoteMessage.data["item"], remoteMessage.data["user"])
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.

     * @param messageBody FCM message body received.
     */
    private fun sendNotification(item: ItemModel?,
                                 user: UserModel?,
                                 clickAction: String?,
                                 remoteMessageItemPayload: String?,
                                 remoteMessageUserPayload: String?) {
        //if session   exists
        val intent: Intent
        val session = SessionManager(applicationContext)


//        startActivityForResult(intent, HttpHandler.MESSAGE_RESULT_CODE)

        if (session!!.checkLogin() != null) {
//            intent = Intent(this, MessageActivity::class.java)
            intent = Intent(clickAction)
            intent.putExtra("item", remoteMessageItemPayload)
            intent.putExtra("user", remoteMessageUserPayload)
        }
        else intent = Intent(this, LoginActivity::class.java)

//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT)

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this).
                setSmallIcon(R.drawable.ic_stat_ic_launcher_custom_transparent).
                setColor(Color.parseColor("#8b5f42")).
                setContentTitle("Pass It On").setContentText("Message arrived for " + item!!.title).
                setAutoCancel(true).setSound(defaultSoundUri).
                setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notifyId = Integer.valueOf(String.format("%1$05d", item!!.id) + String.format("%1$05d", user!!.id))
//        notificationManager.cancel(notifyId)
        notificationManager.notify(notifyId, notificationBuilder.build())
        Log.d(TAG, "message received")
    }

    companion object {

        private val TAG = "MyFirebaseMsgService"
    }
}
