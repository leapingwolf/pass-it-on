package de.com.pass_it_on.network

import de.com.pass_it_on.models.MessageResponse

/**
 * Created by akshata on 30/08/16.
 */
interface MessagePassingInterface{
    fun passMessageList(messages: Array<MessageResponse>)
}