package de.com.pass_it_on.network

import android.app.Activity
import android.content.Context
import android.util.Log
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.com.pass_it_on.models.ImageModel
import de.com.pass_it_on.models.ItemModel
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.FileInputStream
import java.net.URLConnection
import java.util.*

/**
 * Created by akshata on 06/12/2016.
 */

class ItemDataManager {

    companion object {
        val ALL_ITEMS = HttpHandler.API_URL + "/auth/items"
        final val TAG: String = "ItemDataManager"


        fun put(url: String, item: ItemModel?, authToken: String): Result<Any, FuelError>? {
            val contentPair = Pair("Content-Type", "application/json")
            val authPair = Pair("Authorization", authToken)
            val gson = Gson()
            val hashMap = HashMap<String, Any>()
            val item = item as Any

            hashMap.put("item", item)
            val i = item as ItemModel
            val mapType = object : TypeToken<HashMap<String, Any>>() {}.type

            val itemjson = gson.toJson(hashMap, mapType)
            Log.d(HttpHandler.TAG, "json is " + (itemjson))
            val (request, response, result) = Fuel.put(url)
                    .header(contentPair, authPair)
                    .body(itemjson)
                    .responseObject(ItemModel.ObjectDeserializer())
            return result
        }

        fun delete( url: String, itemId: Int, authToken: String): Result<Any, FuelError>? {
            try {
                val contentPair = Pair("Content-Type", "application/json")
                    val authorizationPair = Pair("Authorization", authToken)
                    val(request, response, result) = Fuel.delete("$url/$itemId").header(contentPair, authorizationPair).response()
                    return result
            }
            catch(ex: Exception) {
                Log.d(TAG,"in exception")
                ex.printStackTrace()
            }
            return null
        }



        fun postImage(imageUploadUrl: String, image: ImageModel?, itemId: Int, authToken: String, responseImageId: Int?): Result<ByteArray, FuelError> {
            val header = Pair("Content-Type", "multipart/form-data;boundary=" + "xxxxxxxx")
            val encodeHeader = Pair("Accept-Encoding", "gzip, deflate")
            val authPair = Pair("Authorization",authToken)
            val requestString = writeFile(image, responseImageId)
            val (request, response, result) = Fuel.post("$imageUploadUrl/$itemId/uploadImage")
                    .header(header, encodeHeader, authPair)
                    .body(requestString)
                    .response()
            return result
        }

        private fun writeFile(image: ImageModel?, responseImageId: Int?): ByteArray{
            val hashMap = HashMap<String, Any>()
            val gson = Gson()
            var dataStream: ByteArrayOutputStream? = null
            var fileInputStream: FileInputStream? = null
            hashMap.put("image", image as Any)

            val mapType = object : TypeToken<HashMap<String, Any>>() {}.type
            try {
                val imageIdjson = JSONObject()
                imageIdjson.put("id", responseImageId)

                val imagejson = gson.toJson(hashMap, mapType)
                Log.d(HttpHandler.TAG, "json is " + (imagejson))

                val CRLF = "\r\n"
                val boundary = "xxxxxxxx";


                var progressCallback: ((Long, Long) -> Unit)? = null
                val BUFFER_SIZE = 1024 * 1024
                //val file = sourceCallback.invoke(request, request.url)
                //file input
                val fileNameWithlocalPath = image!!.fileNameWithlocalPath;
                val endIndex = image!!.filename!!.lastIndexOf(".");
                var imagefileName = image!!.filename
                if (endIndex != -1) { imagefileName = image!!.filename!!.substring(0, endIndex) }

                fileInputStream = FileInputStream(fileNameWithlocalPath)
                dataStream = ByteArrayOutputStream().apply {
                    write(("--" + boundary + CRLF).toByteArray())
                    write(("Content-Disposition: form-data; name=\"" + "picture" + "\"; filename=\"" + imagefileName + "\"").toByteArray())
                    write(CRLF.toByteArray())
                    write(("Content-Type: " + URLConnection.guessContentTypeFromName(imagefileName)).toByteArray())
                    write(CRLF.toByteArray())
                    write(CRLF.toByteArray())
                    flush()

                    //input file data
                    fileInputStream!!.copyTo(this, BUFFER_SIZE)

                    //testing
                    write(CRLF.toByteArray())
                    write(("--" + boundary + CRLF).toByteArray())
                    write(("Content-Disposition: form-data; name=\"" + "image" + "\"").toByteArray())
                    write(CRLF.toByteArray())
                    write(("Content-Type: application/json").toByteArray())
                    write(CRLF.toByteArray())
                    write(CRLF.toByteArray())
                    write((imagejson).toByteArray())
                    //tested

                    write(CRLF.toByteArray())
                    write(("--" + boundary + CRLF).toByteArray())
                    write(("Content-Disposition: form-data; name=\"" + "id" + "\"").toByteArray())
                    write(CRLF.toByteArray())
                    write(("Content-Type: application/json").toByteArray())
                    write(CRLF.toByteArray())
                    write(CRLF.toByteArray())
                    write((imageIdjson).toString().toByteArray())

                    write(CRLF.toByteArray())
                    flush()
                    write(("--$boundary--").toByteArray())
                    write(CRLF.toByteArray())
                    flush()
                }
            }
            catch(ex: Exception){
                ex.printStackTrace()
                throw  ex
            }
            finally {
                dataStream?.close()
                fileInputStream?.close()
            }
            return dataStream!!.toByteArray()
        }

    }
}