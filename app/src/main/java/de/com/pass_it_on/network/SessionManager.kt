package de.com.pass_it_on.network


/**
 * Created by akshata on 09/06/16.
 */

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor
import android.util.Log
import de.com.pass_it_on.activities.LoginActivity
import de.com.pass_it_on.models.UserModel
import java.util.*

class SessionManager// Constructor
(// Context

        internal var _context: Context) {
    // Shared Preferences
    internal var pref: SharedPreferences

    // Editor for Shared preferences
    internal var editor: Editor

    // Shared pref mode
    internal var PRIVATE_MODE = 0

    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun createLoginSession(id: Int?, name: String, email: String, token: String) {
        Log.d("SessionManager", "token is " + token)
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true)

        //Storing user id
        editor.putInt(ID, id!!)
        // Storing name in pref
        editor.putString(KEY_NAME, name)

        // Storing email in pref
        editor.putString(KEY_EMAIL, email)

        editor.putString(KEY_TOKEN, token)

//        editor.putString(KEY_FCM_TOKEN, fcmToken)
        // commit changes
        editor.commit()
    }

    //user id
    // user name
    // user email id
    //user current token
    // return user

    private fun getCurrentSession(): UserModel? {
//        userDetails
//        val session = SessionManager(_context)
//        if(session!!.isLoggedIn) {
            val user = userDetails
            val id = user[KEY_ID]!!.toInt()
            val name = user[KEY_NAME]!!.toString()
            val email = user[KEY_EMAIL]!!.toString()
//            val token = user[SessionManager.KEY_TOKEN]!!.toString()
            return UserModel(id, name, email, false)
//        }
//        else{
//            return null
//        }
    }


    val userDetails: HashMap<String, String>
        get() {
            val user = HashMap<String, String>()
            user.put(KEY_ID, pref.getInt(KEY_ID, 0).toString())
            user.put(KEY_NAME, pref.getString(KEY_NAME, null))
            user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null))
            user.put(KEY_TOKEN, pref.getString(KEY_TOKEN, null))
            return user
        }

    val isLoggedIn: Boolean
        get() = pref.getBoolean(IS_LOGIN, false)

    fun logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear()
        editor.commit()

        // After logout redirect user to Loing Activity
        val i = Intent(_context, LoginActivity::class.java)
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)

        // Add new Flag to start new Activity
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK

        // Staring Login Activity
        _context.startActivity(i)
    }

    fun saveFCMToken(token: String){
        editor.putString(KEY_FCM_TOKEN, token)
    }

    fun getFCMToken(): String?{
        return pref.getString(KEY_FCM_TOKEN, null)
    }

    fun checkLogin() : UserModel?{
        // Check login status
        if (!this.isLoggedIn) {
            // user is not logged in redirect him to Login Activity
            val i = Intent(_context, LoginActivity::class.java)
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            // Add new Flag to start new Activity
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            // Staring Login Activity
            _context.startActivity(i)

        }
        else{
            val userModel = getCurrentSession()
            return userModel
        }
        return null
    }

    companion object {

        // Sharedpref file name
        private val PREF_NAME = "AndroidHivePref"

        // All Shared Preferences Keys
        private val IS_LOGIN = "IsLoggedIn"
        private val ID = "id"

        // User name (make variable public to access from outside)
        val KEY_ID = "id"
        val KEY_NAME = "name"

        // Email address (make variable public to access from outside)
        val KEY_EMAIL = "email"
        val KEY_TOKEN = "token"
        val KEY_FCM_TOKEN = "fcmtoken"
    }
}
