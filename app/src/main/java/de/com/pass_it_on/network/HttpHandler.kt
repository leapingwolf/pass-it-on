package de.com.pass_it_on.network

//import nl.komponents.kovenant.Promise
//import nl.komponents.kovenant.deferred
//import nl.komponents.kovenant.task
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.util.Log
import android.widget.ProgressBar
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.google.android.gms.common.api.GoogleApiClient
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.squareup.picasso.Picasso
import de.com.pass_it_on.models.*
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.FileInputStream
import java.net.URLConnection
import java.util.*


/**
 * Created by akshatashan on 3/23/16.
 */


class HttpHandler{

    companion object {
        final val TAG : String = "HttpHandler"
        var googleApiClient: GoogleApiClient? = null
        var pgbar: ProgressBar? = null

//        val API_URL = "http://192.168.1.106:4000"
        val API_URL = "http://pass-it-on.com.de"
//        val API_URL = "http://52.29.161.97:4000"
//         val API_URL = "http://192.168.1.106:4000"
//        val ALL_USERS = API_URL + "/auth/users"
//        val ALL_ITEMS = API_URL + "/auth/items"
        val UPDATE = 1
        val BACK_FROM_IMAGE_UPLOADS_SAVE = 3
        val BACK_FROM_IMAGE_UPLOADS_NO_SAVE = 4
        val NO_UPDATE = 2
        val MESSAGE_RESULT_CODE = 5


        fun errors(): HashMap<String, String> {
            val hashMap = HashMap<String, String>()
            hashMap.put("inactive_user", "An activation email has been sent to the email address.Please check your email to complete the registration")
            hashMap.put("password_does_not_match","Password entered is not correct")
            hashMap.put("password_required","Please enter a password")
            hashMap.put("password_confirmation_does_not_match","Password and Password Confirmation do not match")
            hashMap.put("insufficient_info_email","The provided email address is not registered.Please register from the registration screen")
            hashMap.put("insufficient_info_name","The email address is not registered.Please go to registration")
            hashMap.put("user_already_registered_not_active", "The email address is already registered.Please check your email to complete the registration process")
            hashMap.put("user_already_registered", "The email address is already registered.Please go to the log in screen")
            return hashMap
        }

        fun get(url: String, authToken: String, query: String?, orderBy: Int) : Array<MessageModel>? {
            val contentPair = Pair("Content-Type", "application/json")
            val authPair = Pair("Authorization", authToken)
            val result = Fuel.get(url)
                    .header(contentPair, authPair)
                    .responseObject(MessageModel.Deserializer()).third
            return result.component1()

        }

        fun getMyItems(url: String, pageNumber: Int, authToken: String, query: String?, orderBy: Int): Result<Array<ItemModel>, FuelError>? {
            var result: Result<Array<ItemModel>, FuelError>? = null
            try {
                val contentPair = Pair("Content-Type", "application/json")
                val authPair = Pair("Authorization", authToken)
                var getUrl: String
                //this code executes for all items and my items
                if (query!!.length > 0) getUrl = "$url?page=$pageNumber&where=$query&orderBy=$orderBy"
                else getUrl = "$url?page=$pageNumber&orderBy=$orderBy"
                Log.d(TAG, "get url is:" + getUrl)
                result = Fuel.get(getUrl)
                        .header(contentPair, authPair)
                        .responseObject(ItemModel.Deserializer()).third
                return result
            }
            catch(ex: Exception){
                Log.d(TAG, "error" + ex.printStackTrace())
            }

            return null
        }

        fun getItems(url: String, pageNumber: Int, authToken: String, query: String?,latitude: String?, longitude: String?, orderBy: Int): Result<Array<ItemUserModel>, FuelError>? {
            val contentPair = Pair("Content-Type", "application/json")
            val authPair = Pair("Authorization", authToken)
            var getUrl: String
            val result: Result<Array<ItemUserModel>, FuelError>?
            if (query!!.length > 0) getUrl = "$url?page=$pageNumber&where=$query&orderBy=$orderBy"
            else getUrl = "$url?page=$pageNumber&orderBy=$orderBy"

            if(latitude != null && longitude != null){
                getUrl = "$getUrl&latitude=$latitude&longitude=$longitude"
            }
            Log.d(TAG, "get url is:" + getUrl)
            result = Fuel.get(getUrl)
                    .header(contentPair, authPair)
                    .responseObject(ItemUserModel.Deserializer()).third
            return result
        }

        fun getConversations(url: String, pageNumber: Int, authToken: String,  orderBy: Int): Result<Array<ConversationResponse>, FuelError>? {
            val contentPair = Pair("Content-Type", "application/json")
            val authPair = Pair("Authorization", authToken)
            var getUrl: String
            val result: Result<Array<ConversationResponse>, FuelError>?

                    getUrl = "$url?page=$pageNumber&orderBy=$orderBy"
                    Log.d(TAG, "URL IS" + getUrl)
                    result = Fuel.get(getUrl)
                            .header(contentPair, authPair)
                            .responseObject(ConversationResponse.Deserializer()).third
                    return result
            }
    }

}