package de.com.pass_it_on.network

import android.util.Log
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.com.pass_it_on.models.FcmtokenModel
import java.util.*

/**
 * Created by akshata on 06/12/2016.
 */

class FcmtokenDataManager {

    companion object {
        val FCMTOKENS = HttpHandler.API_URL + "/auth/fcmtokens"
        final val TAG: String = "FcmtokenDataManager"

        //post fcm token
        fun post(url: String, fcmToken: FcmtokenModel?, authToken: String): Result<Any, FuelError>? {
            val contentPair = Pair("Content-Type", "application/json")
            val authPair = Pair("Authorization", authToken)
            val gson = Gson()
            val hashMap = HashMap<String, Any>()
            val fcmToken = fcmToken as FcmtokenModel

            hashMap.put("fcmtoken", fcmToken)
            val mapType = object : TypeToken<HashMap<String, FcmtokenModel>>() {}.type

            val fcmTokenJson = gson.toJson(hashMap, mapType)
            Log.d(HttpHandler.TAG, "json is " + (fcmTokenJson))
            val (request, response, result) = Fuel.post(url)
                    .header(contentPair, authPair)
                    .body(fcmTokenJson)
                    .responseObject(FcmtokenModel.ObjectDeserializer())
            return result
        }

        // log out user
        fun delete( url: String, authToken: String?, fcmToken: String?, userId: Int?): Result<Any, FuelError>? {
            try {
                val contentPair = Pair("Content-Type", "application/json")
                val fcmToken = FcmtokenModel(null,fcmToken,userId)
                val gson = Gson()
                val hashMap = HashMap<String, Any>()
                hashMap.put("fcmtoken", fcmToken)
                val mapType = object : TypeToken<HashMap<String, FcmtokenModel>>() {}.type

                val fcmTokenJson = gson.toJson(hashMap, mapType)
                if(authToken == null){
                    Log.d(FcmtokenDataManager.TAG, "no token")
                }
                else{
                    val authPair = Pair("Authorization", authToken)
                    val(request, response, result) = Fuel.delete(url)
                            .header(contentPair,authPair)
                            .body(fcmTokenJson)
                            .response()
                    return result
                }
            }
            catch(ex: Exception) {
                Log.d(HttpHandler.TAG,"in exception")
                ex.printStackTrace()
            }
            return null
        }
    }
}