package de.com.pass_it_on.network

import android.util.Log
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.com.pass_it_on.models.ItemModel
import de.com.pass_it_on.models.RegistrationResponse
import de.com.pass_it_on.models.RegistrationUser
import java.util.*

/**
 * Created by akshata on 06/12/2016.
 */
class UserDataManager {
    companion object {
        final val TAG: String = "UserDataManager"
        val ALL_USERS = HttpHandler.API_URL + "/auth/users"
        val LOGOUT = HttpHandler.API_URL + "/auth/logout"
        val FB_LOGIN = HttpHandler.API_URL + "/auth/facebook"
        val FB_LOGIN_CALLBACK = HttpHandler.API_URL + "/auth/facebook/callback"
        val IDENTITY_CALLBACK = HttpHandler.API_URL + "/auth/identity/callback"

        // register a user
        fun post(url: String, user: RegistrationUser?): Result<Any, FuelError>? {
            try {
                val contentPair = Pair("Content-Type", "application/json")
                val gson = Gson()
                val hashMap = HashMap<String, Any>()
                val user = user as Any

                hashMap.put("user", user)
                val mapType = object : TypeToken<HashMap<String, Any>>() {}.type
                val userJson = gson.toJson(hashMap, mapType)
                Log.d(HttpHandler.TAG, "json is " + (userJson))
                val (request, response, result) = Fuel.post(url)
                        .header(contentPair)
                        .body(userJson)
                        .responseObject(RegistrationResponse.ObjectDeserializer())

                return result
            } catch(ex: Exception) {
                Log.d(HttpHandler.TAG, ex.message.toString())
            }
            return null
        }

        // log out user
        fun delete( url: String, token: String?): Result<Any, FuelError>? {
            try {
                val contentPair = Pair("Content-Type", "application/json")
                if (token == null){
                    Log.d(HttpHandler.TAG, "no token")
                }
                else{
                    val authorizationPair = Pair("Authorization", token)
                    val(request, response, result) = Fuel.delete(url).header(contentPair, authorizationPair).response()
                    return result
                }
            }
            catch(ex: Exception) {
                Log.d(HttpHandler.TAG,"in exception")
                ex.printStackTrace()
            }
            return null
        }


        fun postItem(url: String, item: ItemModel?,userId: Int, authToken: String): Result<Any, FuelError>?{
            try {
                val contentPair = Pair("Content-Type", "application/json")
                val authPair = Pair("Authorization",  authToken)
                val gson = Gson()
                val hashMap = HashMap<String, Any>()
                val images = item!!.image
                val itemId = item!!.id
                //images.add(ImageModel("test url for image", "test local path for image"))
                val item = item as Any

                hashMap.put("item", item)
                val mapType = object : TypeToken<HashMap<String, Any>>() {}.type
                var imageResult: Result<ByteArray, FuelError>
                val itemjson = gson.toJson(hashMap, mapType)
                Log.d(HttpHandler.TAG, "json is " + (itemjson))
                val(request, response, result) = Fuel.post("$url/$userId/items")
                        .header(contentPair, authPair)
                        .body(itemjson)
                        .responseObject(ItemModel.ObjectDeserializer())
                Log.d(HttpHandler.TAG, "return result")
                return result
            }
            catch(ex: Exception){
                Log.d(HttpHandler.TAG, ex.message.toString())
            }
            return null
        }
    }


}