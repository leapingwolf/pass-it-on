package de.com.pass_it_on.network

import android.util.Log
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.com.pass_it_on.models.ConversationModel
import de.com.pass_it_on.models.MessageResponse
import java.util.*

/**
 * Created by akshata on 06/12/2016.
 */
class ConversationDataManager {

    companion object {
        val CONVERSATIONS = HttpHandler.API_URL + "/auth/conversations"
        final val TAG: String = "ConversationDataManager"

        //get messages for a conversation id
        fun get(url: String, authToken: String, conversationId: Int): Result<Array<MessageResponse>, FuelError>? {
            val contentPair = Pair("Content-Type", "application/json")
            val authPair = Pair("Authorization", authToken)
            try {
                val result = Fuel.get("$url/$conversationId")
                        .header(contentPair, authPair)
                        .responseObject(MessageResponse.Deserializer()).third
                return result
            } catch (ex: Exception) {
                Log.d(HttpHandler.TAG, ex.message)
            }
            return null
        }

        //get messages for a conversation between sender and receiver for an item
        fun get(url: String, authToken: String, senderId: Int?, receiverId: Int?, itemId: Int?): Result<Array<MessageResponse>, FuelError>? {
            val contentPair = Pair("Content-Type", "application/json")
            val authPair = Pair("Authorization", authToken)
            try {
                val result = Fuel.get("$url?sender_id=$senderId&receiver_id=$receiverId&item_id=$itemId")
                        .header(contentPair, authPair)
                        .responseObject(MessageResponse.Deserializer()).third
                return result
            } catch (ex: Exception) {
                Log.d(HttpHandler.TAG, ex.message)
            }
            return null
        }

        //upsert the conversation.
        fun put(url: String, conversation: ConversationModel?, authToken: String): Result<Array<MessageResponse>, FuelError>? {
            val contentPair = Pair("Content-Type", "application/json")
            val authPair = Pair("Authorization", authToken)
            val gson = Gson()
            val hashMap = HashMap<String, Any>()
            val conversation = conversation as Any

            hashMap.put("conversation", conversation)
            val i = conversation as ConversationModel
            val mapType = object : TypeToken<HashMap<String, Any>>() {}.type

            val conversationjson = gson.toJson(hashMap, mapType)
            Log.d(HttpHandler.TAG, "json is " + (conversationjson))
            try {
                val result = Fuel.put(url)
                        .header(contentPair, authPair)
                        .body(conversationjson)
                        .responseObject(MessageResponse.Deserializer()).third
                return result
            } catch (ex: Exception) {
                Log.d(HttpHandler.TAG, ex.message)
                ex.printStackTrace()
            }
            return null
        }
    }
}