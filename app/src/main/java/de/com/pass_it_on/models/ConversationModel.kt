package de.com.pass_it_on.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.GsonBuilder
import io.mironov.smuggler.AutoParcelable
import java.sql.Timestamp
import java.util.*

/**
 * Created by akshata on 17/08/16.
 */

data class ConversationModel(
        val id: Int?,
        val sender_id: Int?,
        val receiver_id: Int?,
        val item_id: Int?,
        val updated_at: Timestamp?,
        val message: ArrayList<MessageModel?>) : AutoParcelable {

    class Deserializer : ResponseDeserializable<Array<ConversationModel>> {
        override fun deserialize(content: String): Array<ConversationModel> {
            var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
            return gson.fromJson(content, Array<ConversationModel>::class.java)
        }
    }

        class ObjectDeserializer : ResponseDeserializable<ConversationModel> {
            override fun deserialize(content: String): ConversationModel {
                var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
                return gson.fromJson(content, ConversationModel::class.java)
                //return new Gson().fromJson(s, DummyItem.class);
            }
        }
    }
