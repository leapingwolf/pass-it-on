package de.com.pass_it_on.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.GsonBuilder
import io.mironov.smuggler.AutoParcelable

/**
 * Created by akshata on 09/06/16.
 */
data class RegistrationUser(
        val name: String?,
        val password: String?,
        val password_confirmation: String?,
        val identity: String,
        val email: String) : AutoParcelable {

        class StringDeserializer : ResponseDeserializable<String> {
            override fun deserialize(content: String): String {
                var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
                return gson.fromJson(content, String::class.java)
                //return new Gson().fromJson(s, DummyItem.class);
            }
        }

        class ObjectDeserializer : ResponseDeserializable<RegistrationUser> {
            override fun deserialize(content: String): RegistrationUser {
                var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
                return gson.fromJson(content, RegistrationUser::class.java)
                //return new Gson().fromJson(s, DummyItem.class);
            }
        }
    }
//}
