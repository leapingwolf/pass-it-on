package de.com.pass_it_on.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.GsonBuilder
import io.mironov.smuggler.AutoParcelable

/**
 * Created by akshata on 19/08/16.
 *

*/

data class ConversationResponse(
        val sender_id: Int?,
        val receiver_id: Int?,
        val senderName: String?,
        val receiverName: String?,
        val itemId: Int?,
        val itemTitle: String?,
        val itemOfferType: Int?,
        val itemOwnerId: Int?,
        val itemAvailable:  Boolean?,
        val conversation: ConversationModel?,
        val image: ImageModel?
) : AutoParcelable {

    class Deserializer : ResponseDeserializable<Array<ConversationResponse>> {
        override fun deserialize(content: String): Array<ConversationResponse>
        {
            var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
            return gson.fromJson(content, Array<ConversationResponse>::class.java)
        }
    }
}
