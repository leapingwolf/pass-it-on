package de.com.pass_it_on.models

import io.mironov.smuggler.AutoParcelable

/**
 * Created by akshatashan on 4/5/16.
 */
data class ImageModel(val id: Int?,
                      val url: String?,
                      var fileNameWithlocalPath: String?,
                      var filename: String?,
                      val uploadStatus: Boolean?,
                      var type: String?,
                      var sequence: Int?) : AutoParcelable