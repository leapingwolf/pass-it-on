package de.com.pass_it_on.models

import android.widget.ImageButton
import android.widget.ProgressBar

/**
 * Created by akshata on 18/09/16.
 */

/**
 * Created by akshatashan on 4/5/16.
 */
data class ImageUploadModel(val image: ImageModel, var pgbar: ProgressBar, var retryBtn : ImageButton)