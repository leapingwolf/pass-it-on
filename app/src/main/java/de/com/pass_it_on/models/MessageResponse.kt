package de.com.pass_it_on.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.GsonBuilder
import io.mironov.smuggler.AutoParcelable

/**
 * Created by akshata on 24/08/16.
 */



data class MessageResponse(
        val receiver: String?,
        val sender: String?,
        val message: MessageModel?
) : AutoParcelable {

    class objectDeserializer : ResponseDeserializable<String> {
        override fun deserialize(content: String): String {
            var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
            return gson.fromJson(content, String::class.java)
        }
    }

        class Deserializer : ResponseDeserializable<Array<MessageResponse>> {
            override fun deserialize(content: String): Array<MessageResponse> {
                var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
                return gson.fromJson(content, Array<MessageResponse>::class.java)
                //return new Gson().fromJson(s, DummyItem.class);
            }
        }
    }
