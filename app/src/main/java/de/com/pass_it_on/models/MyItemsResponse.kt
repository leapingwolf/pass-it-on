package de.com.pass_it_on.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.GsonBuilder
import io.mironov.smuggler.AutoParcelable
import java.util.*

/**
 * Created by akshata on 05/09/16.
 */


data class MyItemsResponse(
        val item: ArrayList<ItemModel?>) : AutoParcelable {


    class Deserializer : ResponseDeserializable<MyItemsResponse> {
        override fun deserialize(content: String): MyItemsResponse {
            var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
            return gson.fromJson(content, MyItemsResponse::class.java)
            //return new Gson().fromJson(s, DummyItem.class);
        }
    }
}