package de.com.pass_it_on.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.GsonBuilder
import io.mironov.smuggler.AutoParcelable

/**
 * Created by akshata on 10/06/16.
 */

data class RegistrationResponse(
        val user: UserModel?,
        val error: String,
        val authorization: String?
        ) : AutoParcelable {

    class ObjectDeserializer : ResponseDeserializable<RegistrationResponse> {
        override fun deserialize(content: String): RegistrationResponse {
            var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
            return gson.fromJson(content, RegistrationResponse::class.java)
            //return new Gson().fromJson(s, DummyItem.class);
        }
    }
        }
