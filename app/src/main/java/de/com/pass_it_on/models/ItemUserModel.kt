package de.com.pass_it_on.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.GsonBuilder
import io.mironov.smuggler.AutoParcelable

/**
 * Created by akshatashan on 4/28/16.
 */


data class ItemUserModel(
        val user: UserModel?,
        val item: ItemModel?,
        val distance: Float?) : AutoParcelable {


    class Deserializer : ResponseDeserializable<Array<ItemUserModel>> {
        override fun deserialize(content: String): Array<ItemUserModel> {
            var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
            return gson.fromJson(content, Array<ItemUserModel>::class.java)
            //return new Gson().fromJson(s, DummyItem.class);
        }
    }
}