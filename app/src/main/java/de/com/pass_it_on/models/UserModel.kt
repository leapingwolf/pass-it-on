package de.com.pass_it_on.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.GsonBuilder
import io.mironov.smuggler.AutoParcelable
/**
 * Created by akshatashan on 4/28/16.
 */


data class UserModel(
        val id: Int?,
        val name: String?,
        val email: String?,
        val anonymity: Boolean?) : AutoParcelable {

    class Deserializer : ResponseDeserializable<Array<UserModel>> {
        override fun deserialize(content: String): Array<UserModel> {
            var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
            return gson.fromJson(content, Array<UserModel>::class.java)
            //return new Gson().fromJson(s, DummyItem.class);
        }
    }
}