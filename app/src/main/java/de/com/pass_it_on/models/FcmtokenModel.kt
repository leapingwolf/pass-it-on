package de.com.pass_it_on.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.GsonBuilder
import io.mironov.smuggler.AutoParcelable

/**
 * Created by akshata on 18/11/2016.
 */


data class FcmtokenModel(
        val id: Int?,
        val token: String?,
        val user_id: Int?): AutoParcelable {


    class Deserializer : ResponseDeserializable<Array<FcmtokenModel>> {
        override fun deserialize(content: String): Array<FcmtokenModel> {
            var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
            return gson.fromJson(content, Array<FcmtokenModel>::class.java)
            //return new Gson().fromJson(s, DummyItem.class);
        }
    }

    class ObjectDeserializer : ResponseDeserializable<FcmtokenModel> {
        override fun deserialize(content: String): FcmtokenModel {
            var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
            return gson.fromJson(content, FcmtokenModel::class.java)
            //return new Gson().fromJson(s, DummyItem.class);
        }
    }
}