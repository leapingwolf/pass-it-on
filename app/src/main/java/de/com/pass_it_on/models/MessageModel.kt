package de.com.pass_it_on.models

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.GsonBuilder
import io.mironov.smuggler.AutoParcelable
import java.sql.Timestamp
/**
 * Created by akshata on 17/08/16.
 */

data class MessageModel(
        val sender_id: Int?,
        val sequence: Int?,
        val receiver_id: Int?,
        val conversation_id: Int?,
        val body: String?,
        val inserted_at: Timestamp?) : AutoParcelable {

        class Deserializer : ResponseDeserializable<Array<MessageModel>> {
        override fun deserialize(content: String): Array<MessageModel> {
            var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
            return gson.fromJson(content, Array<MessageModel>::class.java)
            //return new Gson().fromJson(s, DummyItem.class);
        }
    }
}