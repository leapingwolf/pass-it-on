package de.com.pass_it_on.models

/**
 * Created by akshatashan on 3/24/16.
 */

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.GsonBuilder
import io.mironov.smuggler.AutoParcelable
import java.sql.Timestamp
import java.util.*

/**
 * Created by akshatashan on 3/23/16.
 */

data class ItemModel(
        val id: Int?,
        val title: String?,
        val description: String?,
        val inserted_at: Timestamp?,
        val reservationEndDate: String?,
        val latitude: Float?,
        val longitude: Float?,
        val updated_at: Timestamp?,
        val available: Boolean?,
        val offerType: Int?,
        val image: ArrayList<ImageModel?>?) : AutoParcelable {

    class Deserializer : ResponseDeserializable<Array<ItemModel>> {
        override fun deserialize(content: String): Array<ItemModel> {
            var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
            return gson.fromJson(content, Array<ItemModel>::class.java)
            //return new Gson().fromJson(s, DummyItem.class);
        }
    }

    class ObjectDeserializer : ResponseDeserializable<ItemModel> {
        override fun deserialize(content: String): ItemModel {
            var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
            return gson.fromJson(content, ItemModel::class.java)
            //return new Gson().fromJson(s, DummyItem.class);
        }
    }
}
