/**
 * Copyright 2016 Google Inc. All Rights Reserved.

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

 * http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.com.pass_it_on

import android.util.Log
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService
import de.com.pass_it_on.models.FcmtokenModel
import de.com.pass_it_on.network.FcmtokenDataManager
import de.com.pass_it_on.network.HttpHandler
import de.com.pass_it_on.network.SessionManager
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread


class MyFirebaseInstanceIDService : FirebaseInstanceIdService() {

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]

    override fun onTokenRefresh() {
        // Get updated InstanceID token.
        Log.d(TAG,"on token refresh")
        val refreshedToken = FirebaseInstanceId.getInstance().token
        if(refreshedToken != null){
//        Log.d(TAG, "in refresh token" + refreshedToken!!)
            val session = SessionManager(applicationContext)
            val currentUser = session!!.checkLogin()
            if(currentUser != null) {
                if (session!!.getFCMToken() != refreshedToken) {
                    session!!.saveFCMToken(refreshedToken!!)
                    //send new FCM token to server to save in database
                    val fcmTokenModel = FcmtokenModel(null, refreshedToken, currentUser!!.id)
                    val token = session!!.userDetails[SessionManager.KEY_TOKEN]
                    async() {
                        val result = FcmtokenDataManager.post(FcmtokenDataManager.FCMTOKENS, fcmTokenModel, token!!)
                        result!!.success { Log.d(TAG, "saved successfully") }
                        uiThread {
                            result!!.failure { Log.d(TAG, "error is" + result!!.component2().toString()) }
                        }
                    }
                }
            }
        }
    }

    companion object {
        private val TAG = "MyFirebaseIIDService"
    }
}
