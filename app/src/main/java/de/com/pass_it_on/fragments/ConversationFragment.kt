package de.com.pass_it_on.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import com.squareup.picasso.Picasso
import de.com.pass_it_on.R
import de.com.pass_it_on.activities.HttpInterface
import de.com.pass_it_on.activities.LoginActivity
import de.com.pass_it_on.adapters.MyConversationRecyclerViewAdapter
import de.com.pass_it_on.fragments.ConversationFragment.OnListFragmentInteractionListener
import de.com.pass_it_on.models.ConversationResponse
import de.com.pass_it_on.models.UserModel
import de.com.pass_it_on.network.SessionManager
import de.com.pass_it_on.utils.SimpleDividerItemDecoration
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import java.util.*

/**
 * A fragment representing a list of Items.
 *
 *
 * Activities containing this fragment MUST implement the [OnListFragmentInteractionListener]
 * interface.
 */
class ConversationFragment : Fragment() {
    final val TAG : String = "ConversationFragment"
    // TODO: Customize parameters
    private var mColumnCount = 1
    private var pageNumber =1
    lateinit var mUrl:String
    private var mListener: OnListFragmentInteractionListener? = null
    lateinit var recyclerView: RecyclerView
    private var previousTotal = 0
    private var loading = true
    private val visibleThreshold = 5
    internal var firstVisibleItem: Int = 0
    internal var visibleItemCount:Int = 0
    internal var totalItemCount:Int = 0
    private var callback: HttpInterface? = null
    lateinit var pgbar: ProgressBar
    lateinit var currentUser: UserModel
    private var session: SessionManager? = null
    lateinit var token: String
    var mValues = ArrayList<ConversationResponse>()
    lateinit var sectionLabel : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        session = SessionManager(activity.applicationContext)
        if (session!!.isLoggedIn) {
            val user = session!!.userDetails
            val id = user[SessionManager.KEY_ID]!!.toInt()
            val name = user[SessionManager.KEY_NAME]!!.toString()
            val email = user[SessionManager.KEY_EMAIL]!!.toString()
            token = user[SessionManager.KEY_TOKEN]!!.toString()
            currentUser = UserModel(id, name, email, false)
        } else {
            val toast = Toast.makeText(context, R.string.invalid_user_session, Toast.LENGTH_SHORT)
            toast.show()
            val intent = Intent(activity.baseContext, LoginActivity::class.java)
            startActivity(intent)
            activity.finish()
        }
        if (arguments != null) {
            mColumnCount = arguments.getInt(ARG_COLUMN_COUNT)
            mUrl = arguments.getString(CONV_URL)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_conversation_list, container, false)
        recyclerView = view.findViewById(R.id.conversation_list) as RecyclerView
        pgbar = view.findViewById(R.id.pbHeaderProgress) as ProgressBar
        pgbar.visibility= View.GONE
        sectionLabel= view.findViewById(R.id.section_label) as TextView
        sectionLabel.visibility=View.GONE
        registerForContextMenu(recyclerView)

        // Set the adapter
        val context = recyclerView.context
        if (mColumnCount <= 1) {
            recyclerView.layoutManager = LinearLayoutManager(context)
        } else {
            recyclerView.layoutManager = GridLayoutManager(context, mColumnCount)
        }
        recyclerView.addItemDecoration(SimpleDividerItemDecoration(activity))
        val picasso = Picasso.Builder(context)
                .listener { picasso, uri, exception -> Log.d(TAG, exception.toString()) }
                .build()
        recyclerView.adapter = MyConversationRecyclerViewAdapter(currentUser.id, mValues, mListener, picasso)
//        async(){
//            //TODO remove hardcoded url
//            //var mVal = HttpHandler.get("http://private-1b231-aerialroots.apiary-mock.com/questions#")
//            pageNumber += 1
//            var mVal = callback!!.getItems(pageNumber ,mUrl, "")
//            mValues.addAll(mVal!!.toList())
        Log.d(TAG, "load conversation first time " + pageNumber + "mValues:" + mValues.count() + "mUrl:" + mUrl)
        asyncTask(pageNumber,mUrl,"", 0)
//            uiThread {
//                // view.adapter = ItemRecyclerViewAdapter(mValues, mListener, picasso)
//                recyclerView.adapter = MyItemRecyclerViewAdapter(mValues, mListener, picasso)
//            }
//        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                if (mColumnCount <= 1) {
                    val mLinearLayoutManager = recyclerView!!.layoutManager as LinearLayoutManager
                    totalItemCount = mLinearLayoutManager.itemCount
                    firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition()
                } else {
                    val mGridLayoutManager = recyclerView!!.layoutManager as GridLayoutManager
                    totalItemCount = mGridLayoutManager.itemCount
                    firstVisibleItem = mGridLayoutManager.findFirstVisibleItemPosition()
                }
                visibleItemCount = recyclerView!!.childCount
//                Log.d("ar222", "totalItemCount:" + totalItemCount)
//                Log.d("ar222", "previousTotal:" + previousTotal)
                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false
                        previousTotal = totalItemCount
                    }
                }
                if (!loading && firstVisibleItem + visibleItemCount >= totalItemCount - visibleThreshold) {
                    loading = true
//                    async(){
                    //TODO remove hardcoded url. also handle based on caller.Called from MyItems and Home -> Items tab
                    pageNumber +=  1
                    Log.d(TAG, "load conversations scroll " + pageNumber + "mQuery:" + "")
                    asyncTask(pageNumber,mUrl,"",0)
//                            var mVal = callback!!.getItems(pageNumber , mUrl,"" )
//                            mValues.addAll(mVal!!.toList())
                    //Log.d(TAG, "load items scrolling time " + pageNumber + "mValues:" + mValues.count())
//                            uiThread {
//                                recyclerView.adapter.notifyDataSetChanged()
//                            }
//                        }
                }
            }
        })
        return view
    }


    fun asyncTask(pageNumber: Int, mUrl: String, query: String?, orderBy: Int) {
        pgbar.visibility = 1
        async() {
            val result = callback!!.getConversations(mUrl,pageNumber,  orderBy)
            Log.d(TAG, "pg:" + pageNumber + " query:" + query + " mUrl:" + mUrl)
            result!!.success {
                Log.d(TAG, "Items fetched successfully")
                    var mVal = result!!.component1()
                activity.runOnUiThread() {
                    if (pageNumber == 1) {
                        mValues.clear()
                    }
                    mValues.addAll(mVal!!.toList())
                    activity.runOnUiThread() {
                        pgbar.visibility = View.GONE
                        if (mValues.count() == 0) sectionLabel.visibility = View.VISIBLE
                        else sectionLabel.visibility = View.GONE
                    }
                }
            }
            result!!.failure {
                activity.runOnUiThread {
                    Log.d(TAG, "Items not fetched")
                    pgbar.visibility = View.GONE
                    val toast = Toast.makeText(context, R.string.ConnectionError, Toast.LENGTH_SHORT)
                    toast.show()
                }
            }
            uiThread {
                Log.d(TAG, "refresh view")
                recyclerView.adapter.notifyDataSetChanged() }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is HttpInterface){
            Log.d(TAG, "context is set")
            callback = context as HttpInterface?
        }
        if (context is OnListFragmentInteractionListener) {
            mListener = context as OnListFragmentInteractionListener?
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    fun update( url: String){
        previousTotal = 0
        pageNumber = 1
        asyncTask(pageNumber, url, "",0)
        recyclerView.smoothScrollToPosition(0)
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: ConversationResponse)
    }

    companion object {

        // TODO: Customize parameter argument names
        private val ARG_COLUMN_COUNT = "column-count"
        private val CONV_URL = "conv_url"
        // TODO: Customize parameter initialization
        @SuppressWarnings("unused")
        fun newInstance(columnCount: Int, url: String): ConversationFragment {
            val fragment = ConversationFragment()
            val args = Bundle()
            args.putInt(ARG_COLUMN_COUNT, columnCount)
            args.putString(CONV_URL, url)
            fragment.arguments = args
            return fragment
        }
    }
}
/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
