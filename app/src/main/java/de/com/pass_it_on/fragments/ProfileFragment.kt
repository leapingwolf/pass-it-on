package de.com.pass_it_on.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ListView
import android.widget.TextView
import de.com.pass_it_on.R

/**
 * Created by akshatashan on 3/23/16.
 */
class ProfileFragment : Fragment(){
    final val TAG : String = "ProfileFragment"
    private val ARG_COLUMN_COUNT = "column-count"
    private val ITEM_URL = "item_url"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true

    }
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater!!.inflate(R.layout.fragment_profile, container, false)
        val listView = rootView.findViewById(R.id.profile_list) as ListView
        listView.setOnItemClickListener { adapterView, view, i, l ->
                    Log.d(TAG, "implrmrnt code")

//            ItemListFragment.newInstance(0, "http://private-6a048-test8172.apiary-mock.com/questions")
//            val bundle = Bundle()
////            val date = Date(2340280248)
////            val item = ItemModel(0,"","",date, "", ArrayList<ImageModel?>())
//            bundle.putString(ITEM_URL, "http://private-6a048-test8172.apiary-mock.com/questions")
//            bundle.putInt(ARG_COLUMN_COUNT, 0)
//            val intent = Intent(activity, MyItemsActivity::class.java)
////            intent.putExtras(bundle)
//            startActivity(intent)


            val textView = view as TextView
            Log.d(TAG, textView.text.toString())
        }


        val textView = rootView.findViewById(R.id.section_label) as EditText
        textView.setText("Here is the profile fragment")
        return rootView
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        fun newInstance(sectionNumber: Int): ProfileFragment {
            val fragment = ProfileFragment()
            val args = Bundle()
            args.putInt(ARG_SECTION_NUMBER, sectionNumber)
            fragment.arguments = args
            return fragment
        }
    }
}