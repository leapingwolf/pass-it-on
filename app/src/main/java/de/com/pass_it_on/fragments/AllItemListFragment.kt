package de.com.pass_it_on.fragments

//import com.aerialroots.aerialroots.fragmenttest.dummy.DummyContent.DummyItem
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.MenuItemCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.OnScrollListener
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import com.google.android.gms.location.LocationServices
import com.squareup.picasso.Picasso
import de.com.pass_it_on.App
import de.com.pass_it_on.R
import de.com.pass_it_on.activities.HttpInterface
import de.com.pass_it_on.activities.ItemDetailActivity
import de.com.pass_it_on.adapters.AllItemRecyclerViewAdapter
import de.com.pass_it_on.fragments.AllItemListFragment.OnListFragmentInteractionListener
import de.com.pass_it_on.models.ItemUserModel
import de.com.pass_it_on.utils.NpaLinearLayoutManager
import de.com.pass_it_on.utils.SimpleDividerItemDecoration
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import pl.tajchert.nammu.Nammu
import pl.tajchert.nammu.PermissionListener
import java.util.*


/**
 * A fragment representing a list of Items.
 *
 *
 * Activities containing this fragment MUST implement the [OnListFragmentInteractionListener]
 * interface.
 */
class AllItemListFragment : Fragment(){
    final val TAG : String = "AllItemListFragment"
    private var mCurrentLongitude: Float? =null
    private var mCurrentLatitude: Float? =null



    // TODO: Customize parameters
    private var mColumnCount = 1
    private var pageNumber =1
    private var mQuery: String? = ""
    private var mOrderByQuery: Int = 1
    lateinit var mUrl:String
    var mlistMenu: Int = 0
    private var mListener: OnListFragmentInteractionListener? = null
    lateinit var pgbar: ProgressBar
    private var previousTotal = 0
    private var loading = true
    private val visibleThreshold = 5
    internal var firstVisibleItem: Int = 0
    internal var visibleItemCount:Int = 0
    internal var totalItemCount:Int = 0
    private var callback: HttpInterface? = null
    var mValues = ArrayList<ItemUserModel>()
    lateinit var recyclerView: RecyclerView
    lateinit var sectionLabel : TextView



//    override fun onClose(): Boolean {
//        Log.d(TAG, "on close")
//        return true
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true

        if (arguments != null) {
            mColumnCount = arguments.getInt(ARG_COLUMN_COUNT)
            mUrl = arguments.getString(ITEM_URL)
            mlistMenu = arguments.getInt(MENU)
        }

        setHasOptionsMenu(true)
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView")
        val view = inflater!!.inflate(R.layout.fragment_allitem_list, container, false)
        pgbar = view.findViewById(R.id.pbHeaderProgress) as ProgressBar
        sectionLabel = view.findViewById(R.id.section_label) as TextView
        sectionLabel.visibility= View.GONE
        recyclerView = view.findViewById(R.id.item_list) as RecyclerView
        recyclerView.addItemDecoration(SimpleDividerItemDecoration(activity))
        registerForContextMenu(recyclerView)

        val context = recyclerView.context
        if (mColumnCount <= 1) {
                Log.d("test", "linear layout manager assigned " + recyclerView.isNestedScrollingEnabled)
                recyclerView.layoutManager = NpaLinearLayoutManager(context)
            } else {
                        recyclerView.layoutManager = GridLayoutManager(context, mColumnCount)
                    }
        val picasso = Picasso.Builder(context)
                .listener { picasso, uri, exception -> Log.d(TAG, exception.toString()) }
                .build()

        recyclerView.adapter = AllItemRecyclerViewAdapter(mValues, mListener, picasso)
//        async(){
//            //TODO remove hardcoded url
//            //var mVal = HttpHandler.get("http://private-1b231-aerialroots.apiary-mock.com/questions#")
//            pageNumber += 1
//            var mVal = callback!!.getItems(pageNumber ,mUrl, "")
//            mValues.addAll(mVal!!.toList())
            Log.d(TAG, "load items first time " + pageNumber + "mValues:" + mValues.count() + "mQuery:" + mQuery)
            asyncTask(pageNumber,mUrl,"", mOrderByQuery)
//            uiThread {
//                // view.adapter = ItemRecyclerViewAdapter(mValues, mListener, picasso)
//                recyclerView.adapter = MyItemRecyclerViewAdapter(mValues, mListener, picasso)
//            }
//        }

        recyclerView.addOnScrollListener(object : OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                if (mColumnCount <= 1) {
                    Log.d("test", "linear layout manager assigned in on scroll")
                    val mLinearLayoutManager = recyclerView!!.layoutManager as NpaLinearLayoutManager
                    totalItemCount = mLinearLayoutManager.itemCount
                    firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition()
                } else {
                    val mGridLayoutManager = recyclerView!!.layoutManager as GridLayoutManager
                    totalItemCount = mGridLayoutManager.itemCount
                    firstVisibleItem = mGridLayoutManager.findFirstVisibleItemPosition()
                }
                visibleItemCount = recyclerView!!.childCount
//                Log.d("ar222", "totalItemCount:" + totalItemCount)
//                Log.d("ar222", "previousTotal:" + previousTotal)
                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false
                        previousTotal = totalItemCount
                    }
                }
                if (!loading && firstVisibleItem + visibleItemCount >= totalItemCount - visibleThreshold) {
                    loading = true
//                    async(){
                            //TODO remove hardcoded url. also handle based on caller.Called from MyItems and Home -> Items tab
                            pageNumber +=  1
                            Log.d(TAG, "load items scroll " + pageNumber + "mQuery:" + mQuery)
                            asyncTask(pageNumber,mUrl,mQuery,mOrderByQuery)
//                            var mVal = callback!!.getItems(pageNumber , mUrl,"" )
//                            mValues.addAll(mVal!!.toList())
                            //Log.d(TAG, "load items scrolling time " + pageNumber + "mValues:" + mValues.count())
//                            uiThread {
//                                recyclerView.ad
// apter.notifyDataSetChanged()
//                            }
//                        }
                    }
                }
        })


        //}
        return view
    }

//   override  fun  onCreateContextMenu(menu: ContextMenu, v: View , menuInfo: ContextMenu.ContextMenuInfo) {
//       menu.inflate
////       var search = menu.add(R.string.action_search)
////       search.setShowAsAction(1)
////       search.setIcon(R.drawable.ic_search_black_24dp)
//    }

//     fun swap(list: ArrayList<ItemUserModel>){
//        if (mValues != null) {
//            mValues.clear();
//            mValues.addAll(list);
//        }
//        else {
//            mValues = list;
//        }
//        recyclerView.adapter.notifyDataSetChanged();
//    }

    override fun onContextItemSelected(item: MenuItem) : Boolean{
        Log.d(TAG, "context menu item : " + item.itemId)
        return true
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is HttpInterface){
            Log.d(TAG, "context is set")
            callback = context as HttpInterface?
        }
        if (context is OnListFragmentInteractionListener) {
            mListener = context as OnListFragmentInteractionListener?
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnListFragmentInteractionListener")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater){
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(mlistMenu, menu);
        val sortItem = menu.findItem(R.id.action_sort)
        val searchItem = menu.findItem(R.id.action_search);
        val searchView = MenuItemCompat.getActionView(searchItem) as SearchView
        MenuItemCompat.setOnActionExpandListener(searchItem, object : MenuItemCompat.OnActionExpandListener {
            override fun onMenuItemActionExpand( item: MenuItem): Boolean {
                sortItem.isVisible = false
                return true
            }
            override fun onMenuItemActionCollapse( item: MenuItem): Boolean {
                sortItem.isVisible = true
                //Do whatever you want
                if(mQuery!!.length > 0) {
                    //mQuery has a value when query is submitted.
                    // Query results should position at top irrespective of the earlier position or the recyclerview
                    mQuery = ""
                    previousTotal = 0
                    pageNumber = 1
                    asyncTask(pageNumber, mUrl, "", mOrderByQuery)
                    try {
                        recyclerView.smoothScrollToPosition(0)
                    }catch(ex: Exception){
                        Log.d("test", ex.printStackTrace().toString())
                    }
                }
                return true
            }
        })


        // Get the SearchView and set the searchable configuration
        val searchManager = activity.getSystemService(Context.SEARCH_SERVICE) as SearchManager
        // Assumes current activity is the searchable activity
        searchView.setSearchableInfo(searchManager.getSearchableInfo(activity.componentName));
        searchView.setIconifiedByDefault(false); // Do not iconify the widget; expand it by default
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                val inputManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputManager.hideSoftInputFromWindow(view!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                mQuery = query
                pageNumber =1
                previousTotal = 0
//                Log.d(TAG, "load items query " + pageNumber)
                asyncTask(pageNumber,mUrl,query, mOrderByQuery)
                recyclerView.smoothScrollToPosition(0)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                Log.d(TAG, "on query text change")
                return true
            }

        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState);
//        buildGoogleApiClient()
//        if (mGoogleApiClient != null) {
//            mGoogleApiClient!!.connect()
//        }
    }


//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        Log.d(TAG, "update recyclerview here")
//        asyncTask(1, "/auth/users/3/items", "")
//    }

    fun update( url: String, currentLatitude: Float?, currentLongitude: Float?){
            mCurrentLatitude = currentLatitude
            mCurrentLongitude = currentLongitude
            mQuery = ""
            previousTotal = 0
            pageNumber = 1
            asyncTask(pageNumber, url, mQuery, mOrderByQuery)
            recyclerView.smoothScrollToPosition(0)
        }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "all item list fragment resumed")
        Nammu.permissionCompare(object: PermissionListener {
            override fun permissionsChanged(permissionRevoke: String)
            {
                //Toast.makeText(activity, "Access granted or removed = " + permissionRevoke, Toast.LENGTH_SHORT).show()
            }
            override  fun permissionsGranted(permissionGranted: String){
                //Toast.makeText(activity, "Access granted = " + permissionGranted, Toast.LENGTH_SHORT).show()
                if(App.googleApiHelper.isConnected){
                val location = LocationServices.FusedLocationApi.getLastLocation(App.googleApiHelper.googleApiClient)
                    if(location != null) {
                        //If everything went fine lets get latitude and longitude
                        mCurrentLatitude = location.latitude.toFloat()
                        mCurrentLongitude = location.longitude.toFloat()
                        asyncTask(1, mUrl, mQuery, mOrderByQuery)
                    }

//                    Toast.makeText(activity, currentLatitude.toString() + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
                }
            }
            override fun permissionsRemoved(permissionRemoved: String){
                //Toast.makeText(activity, "Access removed = " + permissionRemoved, Toast.LENGTH_SHORT).show()
                mCurrentLatitude = null
                mCurrentLongitude = null
            }
        })
    }

    fun asyncTask(pageNumber: Int, mUrl: String, query: String?, orderBy: Int) {
        Log.d(TAG, "show progressbar")
        pgbar.visibility = 1
         async() {
            var result: Result<Array<ItemUserModel>, FuelError>?
            if(mCurrentLatitude == null && mCurrentLongitude == null){
            result = callback!!.getAllItems( mUrl, pageNumber, query,null , null, orderBy)
        }
        else{
            result = callback!!.getAllItems( mUrl, pageNumber, query, mCurrentLatitude.toString(), mCurrentLongitude.toString(), orderBy)
        }

            Log.d(TAG, "pg:" + pageNumber + " query:" + query + " mUrl:" + mUrl)
            result!!.success {
            //     Log.d(TAG, "Items fetched successfully")
                var mVal = result!!.component1() as Array<ItemUserModel>
                activity.runOnUiThread {
                        if(pageNumber == 1) {mValues.clear()}
                            mValues.addAll(mVal!!.toList())
                            pgbar.visibility = View.GONE
                            if (mValues.count() == 0) sectionLabel.visibility = View.VISIBLE
                            else sectionLabel.visibility = View.GONE
                }
            }
            result!!.failure {
                activity.runOnUiThread {
                    Log.d(TAG, "gone progressbar 2")
                    pgbar.visibility = View.GONE
                    val toast = Toast.makeText(context, R.string.ConnectionError, Toast.LENGTH_SHORT)
                    toast.show()
                }
            }
            uiThread {
                pgbar.visibility = View.GONE
                recyclerView.adapter.notifyDataSetChanged()
            }

            }
        }

    override fun onPrepareOptionsMenu(menu: Menu){
        super.onPrepareOptionsMenu(menu);
        val sortItem = menu.findItem(R.id.action_sort)
        Log.d(TAG,"setting checked")
        sortItem.subMenu.findItem(R.id.last_updated).isChecked = true
        mOrderByQuery = 1
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //TODO: remove hardcoding search
        if(item.title == resources.getString(R.string.last_updated)){
            item.isChecked = true
            mOrderByQuery = 1
            previousTotal = 0
            pageNumber = 1
            asyncTask(pageNumber, mUrl, mQuery,mOrderByQuery)
            recyclerView.smoothScrollToPosition(0)
        }
        if(item.title == resources.getString(R.string.most_recent)){
            item.isChecked = true
            mOrderByQuery = 0
            Log.d(TAG, "last updated item id " + item.itemId)
            previousTotal = 0
            pageNumber = 1
            asyncTask(pageNumber, mUrl, mQuery,mOrderByQuery)
            recyclerView.smoothScrollToPosition(0)
        }
        if (item.title == resources.getString(R.string.action_add)) {
            val bundle = Bundle()
            bundle.putParcelable("item", null)
            bundle.putInt("menu", R.menu.menu_myitems_additem)

            val intent = Intent(activity, ItemDetailActivity::class.java)
            intent.putExtras(bundle)
            startActivityForResult(intent, 1)

        }
        return true
    }
    override  fun onPause() {
        super.onPause();
    }


    override fun onDetach() {
        Log.d(TAG, "ListFragment detached")
        super.onDetach()
        mListener = null
        callback = null
    }


    override fun onDestroy(){
        Log.d(TAG, "ListFragment destroyed")
        pageNumber = 1
        super.onDestroy()
    }

//    override fun onResume(){
//        super.onResume()
//        Log.d(TAG, "onResume of itemlist is called")
//    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onListFragmentInteraction(item: ItemUserModel){
            Log.i("AR", "my item is selected" + item!!.item!!.id.toString())
//            val bundle = Bundle()
//            bundle.putParcelable("item", null)
//            bundle.putInt("menu", R.menu.menu_myitems_edititem)

//            val intent = Intent(, ItemDetailActivity::class.java)
//            intent.putExtras(bundle)
//            startActivityForResult(intent, 1)
        }

    }

    companion object {

        // TODO: Customize parameter argument names
        private val ARG_COLUMN_COUNT = "column-count"
        private val ITEM_URL = "item_url"
        private val MENU = "listMenu"

        // TODO: Customize parameter initialization
        @SuppressWarnings("unused")
        fun newInstance(columnCount: Int, url: String, listMenu: Int): AllItemListFragment {
            val fragment = AllItemListFragment()
            val args = Bundle()
            args.putInt(ARG_COLUMN_COUNT, columnCount)
            args.putString(ITEM_URL, url)
            args.putInt(MENU, listMenu)
            fragment.arguments = args
            return fragment
        }
    }

//    }
}

/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
