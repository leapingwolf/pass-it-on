package de.com.pass_it_on.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.Toast
import de.com.pass_it_on.R
import de.com.pass_it_on.activities.HttpInterface
import de.com.pass_it_on.activities.LoginActivity
import de.com.pass_it_on.adapters.MessageRecyclerViewAdapter
import de.com.pass_it_on.models.ConversationModel
import de.com.pass_it_on.models.ItemUserModel
import de.com.pass_it_on.models.MessageResponse
import de.com.pass_it_on.models.UserModel
import de.com.pass_it_on.network.SessionManager
import de.com.pass_it_on.utils.SimpleDividerItemDecoration
import java.util.*

/**
 * A fragment representing a list of Items.
 *
 *
 * Activities containing this fragment MUST implement the [OnListFragmentInteractionListener]
 * interface.
 */
class MessageFragment : Fragment() {
    final val TAG : String = "MessageFragment"
    // TODO: Customize parameters
    private var mColumnCount = 1
    private var pageNumber =1
    lateinit var mUrl:String
    private var mCallBack: testInterface? = null
    lateinit var recyclerView: RecyclerView
//    private var previousTotal = 0
//    private var loading = true
//    private val visibleThreshold = 5
//    internal var firstVisibleItem: Int = 0
//    internal var visibleItemCount:Int = 0
//    internal var totalItemCount:Int = 0
    private var callback: HttpInterface? = null
    lateinit var pgbar: ProgressBar
    var itemUser: ItemUserModel? = null
    var conversation: ConversationModel? = null
    var mValues = ArrayList<MessageResponse?>()
    lateinit var currentUser: UserModel
    private var session: SessionManager? = null
    lateinit var token: String

    interface  testInterface{
        fun getConversationData(conversationId: Int?, enquirerId: Int?, itemOwnerId: Int?)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
//        if (arguments != null ) {
////            itemUser = arguments.getParcelable(MessageFragment.ITEM_USER)
////            conversation = arguments.getParcelable(MessageFragment.CONVERSATION)
//            //mValues = arguments.getParcelableArray(MessageFragment.MESSAGE_RESPONSE) as ArrayList<MessageResponse?>
//        }
        session = SessionManager(activity.applicationContext)
        if (session!!.isLoggedIn) {
            val user = session!!.userDetails
            val id = user[SessionManager.KEY_ID]!!.toInt()
            val name = user[SessionManager.KEY_NAME]!!.toString()
            val email = user[SessionManager.KEY_EMAIL]!!.toString()
            token = user[SessionManager.KEY_TOKEN]!!.toString()
            currentUser = UserModel(id, name, email, false)
        } else {
            val toast = Toast.makeText(context, R.string.invalid_user_session, Toast.LENGTH_SHORT)
            toast.show()
            val intent = Intent(activity.baseContext, LoginActivity::class.java)
            startActivity(intent)
            activity.finish()
        }
//        if (arguments != null) {
//            mColumnCount = arguments.getInt(ARG_COLUMN_COUNT)
//            mUrl = arguments.getString(ConversationFragment.CONV_URL)
//        }

    }

    fun update(newMessages: ArrayList<MessageResponse?>){
        if(newMessages!!.count() > 0) {
            mValues.clear()
            mValues.addAll(newMessages)
//            mValues.addAll(newMessages!!.toList())
            recyclerView.adapter.notifyDataSetChanged()
            recyclerView.scrollToPosition(mValues.count())
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        val view = inflater!!.inflate(R.layout.fragment_message_list, container, false)
        recyclerView = view.findViewById(R.id.message_list) as RecyclerView

        registerForContextMenu(recyclerView)

        // Set the adapter
        val context = recyclerView.context
        if (mColumnCount <= 1) {
            recyclerView.layoutManager = LinearLayoutManager(context)
        } else {
            recyclerView.layoutManager = GridLayoutManager(context, mColumnCount)
        }
        recyclerView.addItemDecoration(SimpleDividerItemDecoration(activity))

//        val picasso = Picasso.Builder(context)
//                .listener { picasso, uri, exception -> Log.d("Aerialroots", exception.toString()) }
//                .build()


//        async(){
//            //TODO remove hardcoded url
//            //var mVal = HttpHandler.get("http://private-1b231-aerialroots.apiary-mock.com/questions#")
//            pageNumber += 1
//            var mVal = callback!!.getItems(pageNumber ,mUrl, "")
//            mValues.addAll(mVal!!.toList())
//        Log.d(TAG, "load conversation " + pageNumber + "mValues:" + mValues.count() + "mUrl:" + mUrl)
        //asyncTask()
        recyclerView.adapter = MessageRecyclerViewAdapter(activity, currentUser!!.id, mValues)
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
        })
        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
//        if (context is MessageFragment.testInterface) {
//            mCallBack = context as MessageFragment.testInterface?
//        } else {
//            throw RuntimeException(context!!.toString() + " must implement testInterface")
//        }
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onResume(){
        super.onResume()
        Log.d(TAG,"messge frag resumed")
    }


    companion object {

        // TODO: Customize parameter argument names
        private val ARG_COLUMN_COUNT = "column-count"
        private val ITEM_URL = "message_url"
        private val MENU = "listMenu"
        private val CALLER = "calling_activity"
        private val ITEM_USER = "itemUser"
        private val CONVERSATION = "conversation"
        private val MESSAGE_RESPONSE = "messageResponse"

        // TODO: Customize parameter initialization
        @SuppressWarnings("unused")

        fun newInstance(): MessageFragment {
            val fragment = MessageFragment()
            val args = Bundle()
//            args.putParcelableArrayList(MessageFragment.MESSAGE_RESPONSE,messageResponseArray)
            fragment.arguments = args
            return fragment
        }

//        fun newInstance(columnCount: Int, itemUser: ItemUserModel?): MessageFragment {
//            val fragment = MessageFragment()
//            val args = Bundle()
//            args.putInt(ARG_COLUMN_COUNT, columnCount)
//            args.putParcelable(MessageFragment.ITEM_USER, itemUser)
////            args.putParcelableArrayList(MessageFragment.MESSAGE_RESPONSE,messageResponseArray)
//            fragment.arguments = args
//            return fragment
//        }
//
//        fun newInstance(columnCount: Int, conversation: ConversationModel?): MessageFragment {
//            val fragment = MessageFragment()
//            val args = Bundle()
//            args.putInt(ARG_COLUMN_COUNT, columnCount)
//            args.putParcelable(MessageFragment.CONVERSATION, conversation)
//            fragment.arguments = args
//            return fragment
//        }
    }
}
/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
