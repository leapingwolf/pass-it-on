package de.com.pass_it_on.fragments

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import com.squareup.picasso.Picasso
import de.com.pass_it_on.R
import de.com.pass_it_on.activities.HttpInterface
import de.com.pass_it_on.activities.LoginActivity
import de.com.pass_it_on.adapters.ImageUploadViewAdapter
import de.com.pass_it_on.models.*
import de.com.pass_it_on.network.HttpHandler
import de.com.pass_it_on.network.ItemDataManager
import de.com.pass_it_on.network.SessionManager
import de.com.pass_it_on.utils.AlertDialogFragment
import de.com.pass_it_on.utils.DeviceDimensionsHelper
import de.com.pass_it_on.utils.FragmentBackPressedListener
import org.apache.commons.io.FileUtils
import org.jetbrains.anko.async
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import pl.tajchert.nammu.Nammu
import pl.tajchert.nammu.PermissionCallback
import java.io.File
import java.util.*

/**
 * A fragment representing a list of Items.
 *
 *
 * Activities containing this fragment MUST implement the [OnListFragmentInteractionListener]
 * interface.
 */
class ImageUploadFragment : Fragment(), FragmentBackPressedListener {
    final val TAG : String = "ImageUploadFragment"
    override fun doBack() {
        if (imageViewChanged) {
            val bundle = Bundle()
            bundle.putString("message", resources.getText(R.string.unsaved_changes).toString())
            val newFragment = AlertDialogFragment.newInstance(bundle)
            newFragment.show(fragmentManager, "AlertDialog")
        }
        else{
            activity.setResult(HttpHandler.BACK_FROM_IMAGE_UPLOADS_NO_SAVE)
            activity.finish()
        }
    }

    private fun validateFields(): Boolean {
        Log.d(TAG, "need to check for image file changes-- count increased or filename changes")

        return false
    }

    // TODO: Customize parameters
    private var mColumnCount = 1
    private var pageNumber =1
    lateinit var mUrl:String
    lateinit var recyclerView: RecyclerView
    private val edited: Boolean = false
    //    private var previousTotal = 0
//    private var loading = true
//    private val visibleThreshold = 5
//    internal var firstVisibleItem: Int = 0
//    internal var visibleItemCount:Int = 0
//    internal var totalItemCount:Int = 0
    lateinit var imageUploads: ArrayList<ImageUploadModel>
    lateinit var images: ArrayList<ImageModel>
    private var callback: HttpInterface? = null
    lateinit var item: ItemModel
    var conversation: ConversationModel? = null
    var mValues = ArrayList<ImageUploadModel?>()
    lateinit var currentUser: UserModel
    private var session: SessionManager? = null
    lateinit var token: String
    var seq: Int= 0
    var imagePosition = 0
    var imageViewChanged: Boolean = false
    lateinit  var pgbar: ProgressBar
    lateinit  var retryButton: ImageButton
    lateinit var imageView: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        setHasOptionsMenu(true)
        if (arguments != null) {
            //if(arguments.getParcelable(ITEM) != null) {
//            itemUser = arguments.getParcelable(ITEM_USER)
            session = SessionManager(activity.applicationContext)
            if (session!!.isLoggedIn) {
                val user = session!!.userDetails
                val id = user[SessionManager.KEY_ID]!!.toInt()
                val name = user[SessionManager.KEY_NAME]!!.toString()
                val email = user[SessionManager.KEY_EMAIL]!!.toString()
                token = user[SessionManager.KEY_TOKEN]!!.toString()
                currentUser = UserModel(id, name, email, false)
            } else {
                val toast = Toast.makeText(context, R.string.invalid_user_session, Toast.LENGTH_SHORT)
                toast.show()
                val intent = Intent(activity.baseContext, LoginActivity::class.java)
                startActivity(intent)
                activity.finish()
            }
            //}
            //TODO replace hardcoded currentUser with the logged in user.
//            currentUser = UserModel(3,"Akshata","akshatashan@gmail.com", false)
            item = arguments.getParcelable(ITEM)
            images = arguments.getParcelableArrayList<ImageModel>(IMAGES)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Log.d(TAG, "inflate image uplod fragment-- should happen only once")
        val view = inflater!!.inflate(R.layout.fragment_image_upload_list, container, false)
        recyclerView = view.findViewById(R.id.my_recycler_view) as RecyclerView
        recyclerView.setHasFixedSize(true);

        val layoutManager = LinearLayoutManager(activity)
        recyclerView.layoutManager = layoutManager
        registerForContextMenu(recyclerView)

        // Set the adapter
        val context = recyclerView.context
        if (mColumnCount <= 1) {
            recyclerView.layoutManager = LinearLayoutManager(context)
        } else {
            recyclerView.layoutManager = GridLayoutManager(context, mColumnCount)
        }

        imageUploads = ArrayList<ImageUploadModel>(5)
//        progressBar = ArrayList<ProgressBar>(5)
//        retryButton = ArrayList<ImageButton>(5)


        var i = 0
        while(i<5) {
            if (images == null) {
                val image = ImageModel(null, null, null, null, false, null, null)
                imageUploads.add(ImageUploadModel(image, ProgressBar(activity), ImageButton(activity)))
            }
            i += 1
        }

        i=0
        var blnAdded = false
        if(images != null) {
            while (i < 5) {
                blnAdded = false
                for (image in images) {
                    if (i + 1 == image.sequence) {
                        Log.d(TAG, "add image")
                        blnAdded = true
                        imageUploads.add(ImageUploadModel(image, ProgressBar(activity), ImageButton(activity)))
                        break
                    }
                }
                if (!blnAdded) {
                    Log.d(TAG, "add empty")
                    val image = ImageModel(null, null, null, null, false, null, null)
                    imageUploads.add(ImageUploadModel(image, ProgressBar(activity), ImageButton(activity)))
                }
                i += 1
            }
        }


//            if(images != null) {
//                if (i < images.count()) {
//                    val image = images[i]
//                    if(image.sequence == i) {
//                        imageUploads.add(ImageUploadModel(image, ProgressBar(activity), ImageButton(activity)))
//                    } else {
//                        val image = ImageModel(null, null, null, null, false, null, null)
//                        imageUploads.add(ImageUploadModel(image, ProgressBar(activity), ImageButton(activity)))
//                    }
//                }
//            }else{
//                val image = ImageModel(null, null, null, null, false, null, null)
//                imageUploads.add(ImageUploadModel(image, ProgressBar(activity), ImageButton(activity)))
//            }
//            i += 1
////            val image = ImageModel(null,null,null,null,false,null,null
//        }


        recyclerView.setHasFixedSize(true)

        Log.d(TAG, "recyclerview")
            recyclerView.adapter = ImageUploadViewAdapter(activity, imageUploads, object : ImageUploadViewAdapter.ImageUploadAdapterListener {
                override fun cameraBtnOnClick(v: View, position: Int) {
                    openEasyImageCamera()
                    imagePosition = position
                    imageUploads[imagePosition].pgbar =  v.findViewById(R.id.pgbar) as ProgressBar
                    imageUploads[imagePosition].retryBtn = v.findViewById(R.id.btnRetry) as ImageButton
                    imageView = v.findViewById(R.id.imageView) as ImageView
                }

                override fun retryBtnOnClick(v: View, position: Int) {
                    Log.d(TAG, "retry clicked in fragment")
//                    imagePosition = position
                    uploadImage(imageUploads[position])
//                    progressBar = v.findViewById(R.id.pgbar) as ProgressBar
//                    retryButton = v.findViewById(R.id.btnRetry) as ImageButton
                }

                override fun galleryBtnOnClick(v: View, position: Int) {
                    Log.d(TAG, "gallery clicked in fragment")
                    openEasyImageGallery()
                    imagePosition = position
                    imageUploads[imagePosition].pgbar =  v.findViewById(R.id.pgbar) as ProgressBar
                    imageUploads[imagePosition].retryBtn = v.findViewById(R.id.btnRetry) as ImageButton
                    imageView = v.findViewById(R.id.imageView) as ImageView
                }
            })
//        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//        })
        return view
    }

    private fun openEasyImageGallery() {
        EasyImage.openGallery(this,0)
    }

    private fun openEasyImageCamera() {
        val permissionCheck = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
            EasyImage.openCamera(this, 0)
        } else {
            Nammu.askForPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE, object : PermissionCallback {
                override fun permissionGranted() {
                    EasyImage.openCamera(activity, 0)
                }

                override fun permissionRefused() {
                    Log.d(TAG, "permission for camera denied")
                }

            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, object : DefaultCallback() {
            override fun onImagePickerError(e: Exception?, source: EasyImage.ImageSource?, type: Int) {
                //Some error handling
            }

            override fun onImagePicked(imageFile: File, source: EasyImage.ImageSource, type: Int) {
                //Handle the image
                onPhotoReturned(imageFile, type)
            }

            override fun onCanceled(source: EasyImage.ImageSource?, type: Int) {
                //Cancel handling, you might wanna remove taken photo if it was canceled
                if (source == EasyImage.ImageSource.CAMERA) {
                    val photoFile = EasyImage.lastlyTakenButCanceledPhoto(activity)
                    photoFile?.delete()
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        Log.d(TAG, "inflate the menu")
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_image_uploadlist, menu);
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.title == resources.getString(R.string.action_done)) {
            activity.setResult(HttpHandler.BACK_FROM_IMAGE_UPLOADS_SAVE)
            activity.finish()
        }
        return true
    }

    private fun onPhotoReturned(photoFile: File, requestCode: Int) {
        val destFile: File? = File(activity.getExternalFilesDir(null).absolutePath, photoFile.name)
//        val destFile = photoFile.name.substring(photoFile.name.lastIndexOf('/') + 1)
        //compress the photoFile
        try {
            val destFile: File? = File(activity.getExternalFilesDir(null).absolutePath, photoFile.name)
            val returnedFile = DeviceDimensionsHelper.compressImage(photoFile.absolutePath, destFile)
            Log.d(TAG, "returned and compressed" + returnedFile)
            FileUtils.deleteQuietly(photoFile)
                if (File(returnedFile).exists()){
                    imageUploads[imagePosition].image.fileNameWithlocalPath =  destFile!!.toString()
                    imageUploads[imagePosition].image.filename = photoFile.name
                    imageUploads[imagePosition].image.type = "png"
                    imageUploads[imagePosition].image.sequence = imagePosition + 1
                    Log.d(TAG, "notifying item added")
                    Picasso.with(activity).load(File(returnedFile)).fit().centerInside().into(imageView)
                    imageViewChanged = true
                    uploadImage(imageUploads[imagePosition])
                }
        } catch(e: Exception) {
            e.printStackTrace()
        }
    }

    private fun uploadImage(imageUpload: ImageUploadModel) {
        Log.d(TAG, "image upload called for position:"  + imageUpload.image.sequence)
//        val imageUploadUrl = "http://192.168.1.106:4000/uploadImage"
        imageUpload.pgbar.visibility= View.VISIBLE
        imageUpload.retryBtn.visibility= View.GONE
        async(){
//            val result = HttpHandler.postImage(context,imageUploadUrl, imageUpload.image, token, null)
            val result = ItemDataManager.postImage(ItemDataManager.ALL_ITEMS , imageUpload.image, item!!.id!!, token, null)
            result!!.success {
                activity.runOnUiThread(){
                    Log.d(TAG,"async task success " + imageUpload.image.sequence)
                    imageUpload.pgbar.visibility= View.GONE
                }
            }
            result!!.failure {
                Log.d(TAG,"async task retry failure for " + imageUpload.image.sequence + " with error " + result!!.component2().toString())
                activity.runOnUiThread(){
                    imageUpload.pgbar.visibility= View.GONE
                    imageUpload.retryBtn.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onResume(){
        super.onResume()
        Log.d(TAG,"messge frag resumed")
    }


    companion object {

        // TODO: Customize parameter argument names
        private val IMAGES = "images"
        private val ITEM = "item"

        // TODO: Customize parameter initialization
        @SuppressWarnings("unused")

        fun newInstance(images: ArrayList<ImageModel>, item: ItemModel): ImageUploadFragment {
            val fragment = ImageUploadFragment()
            val args = Bundle()
            args.putParcelable(ITEM, item)
            args.putParcelableArrayList(IMAGES, images)
//            args.putParcelableArrayList(MessageFragment.MESSAGE_RESPONSE,messageResponseArray)
            fragment.arguments = args

            return fragment
        }

    }
}
/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
