package de.com.pass_it_on.fragments


//import kotlinx.android.synthetic.main.fragment_item_detail.*
import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.synnapps.carouselview.CarouselView
import de.com.pass_it_on.App
import de.com.pass_it_on.R
import de.com.pass_it_on.activities.*
import de.com.pass_it_on.models.ImageModel
import de.com.pass_it_on.models.ItemModel
import de.com.pass_it_on.models.ItemUserModel
import de.com.pass_it_on.models.UserModel
import de.com.pass_it_on.network.HttpHandler
import de.com.pass_it_on.network.ItemDataManager
import de.com.pass_it_on.network.SessionManager
import de.com.pass_it_on.network.UserDataManager
import de.com.pass_it_on.utils.*
import org.jetbrains.anko.*
import pl.tajchert.nammu.Nammu
import pl.tajchert.nammu.PermissionCallback
import pl.tajchert.nammu.PermissionListener
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ItemDetailFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ItemDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ItemDetailFragment() : Fragment(),
        FragmentBackPressedListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {



    final val TAG : String = "ItemDetailFragment"
    // TODO: Rename and change types of parameters
    val GIVE: Int =1
    val NEED: Int = 2
    var permission : Boolean = false
    private var mParam1: String? = null
    private var mParam2: String? = null
    var mdetailMenu: Int = 0
    var mEditable: Boolean = false
    var mAddition: Boolean = false
    var reqObject: ItemModel? = null
    val CAMERA = "camera"
    private val GALLERY = "gallery"
    lateinit var mImageName: TextView
    lateinit var image_resources: IntArray
    private var dotsCount: Int = 0
    lateinit var dots: Array<ImageView>
    private var carouselView: CarouselView? = null
    lateinit var picasso: Picasso
    var available: Boolean? = null
    private var seq: Int? = 0
    private var mListener: OnFragmentInteractionListener? = null
    lateinit var images: ArrayList<ImageModel?>
    private val PLAY_SERVICES_RESOLUTION_REQUEST = 1000
    private var mGoogleApiClient: GoogleApiClient? = null
    private var location: Location? = null
    private var mLocationRequest: LocationRequest? = null
    private var  mapsSupported: Boolean = false
    lateinit var mGoogleApiHelper: GoogleApiHelper
    private val targets = ArrayList<Target>()
    lateinit var titleText: TextView
    lateinit var descriptionText: TextView
    lateinit var postingDetails: TextView
    lateinit var wantitgonebyText: TextView
    lateinit var days: EditText
    lateinit var userIdText: TextView
    //    lateinit var mAttacher: PhotoViewAttacher
    lateinit var btnImageCapture: ImageButton
    lateinit var btnOpenGallery: ImageButton
    lateinit var btnImageUpload: ImageButton
    var blnImageUpload: Boolean = false
    lateinit var availabilitySwitch: Switch
    lateinit var availabilitySwitchstatus: TextView
    lateinit var offerSwitch: Switch
    lateinit var switchStatus: TextView
    var currentOfferType: Int = 0
    private var caller: Int = 0
    private var callback: HttpInterface? = null
    var itemUser: ItemUserModel? = null
    var item: ItemModel? = null
    var user: UserModel? = null
    lateinit var currentUser: UserModel
    private var headerProgress: ProgressBar? = null
    private var session: SessionManager? = null
    lateinit var token: String
    lateinit var mapView: MapView
    private  var map: GoogleMap? = null
    private var latitude: Float? = null
    private var longitude: Float? = null

    var initialImageCnt = 0
    var MESSAGE_RESULT_CODE = 100
    //TODO:
//    private val API_URL = "http://52.58.12.52/"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        retainInstance = true
        if (arguments != null) {
            //if(arguments.getParcelable(ITEM) != null) {
//            itemUser = arguments.getParcelable(ITEM_USER)
            session = SessionManager(activity.applicationContext)
            if (session!!.isLoggedIn) {
                val user = session!!.userDetails
                val id = user[SessionManager.KEY_ID]!!.toInt()
                val name = user[SessionManager.KEY_NAME]!!.toString()
                val email = user[SessionManager.KEY_EMAIL]!!.toString()
                token = user[SessionManager.KEY_TOKEN]!!.toString()
                currentUser = UserModel(id, name, email, false)
            } else {
                val toast = Toast.makeText(context, R.string.invalid_user_session, Toast.LENGTH_SHORT)
                toast.show()
                val intent = Intent(activity.baseContext, LoginActivity::class.java)
                startActivity(intent)
                activity.finish()
            }
            //}
            //TODO replace hardcoded currentUser with the logged in user.
//            currentUser = UserModel(3,"Akshata","akshatashan@gmail.com", false)
            item = arguments.getParcelable(ITEM)
            user = arguments.getParcelable(USER)
            if(user == null){
                user = currentUser
            }
            mdetailMenu = arguments.getInt(MENU)
            mEditable = arguments.getBoolean(EDITABLE)
            mAddition = arguments.getBoolean(ADDITION)
            mParam1 = arguments.getString(ARG_PARAM1)
            mParam2 = arguments.getString(ARG_PARAM2)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        picasso = Picasso.Builder(context).build()
        val view = inflater!!.inflate(R.layout.fragment_item_detail, container, false)
         mapView = view.findViewById(R.id.map) as MapView
        headerProgress = view.findViewById(R.id.pbHeaderProgress) as ProgressBar?
        headerProgress!!.visibility = View.GONE
        offerSwitch= view.findViewById(R.id.offerSwitch) as Switch
        switchStatus= view.findViewById(R.id.switchStatus) as TextView
        availabilitySwitch= view.findViewById(R.id.avaialableSwitch) as Switch
        availabilitySwitchstatus= view.findViewById(R.id.availableSwitchstatus) as TextView
        carouselView = view.findViewById(R.id.carouselView) as CarouselView?
        titleText = view.findViewById(R.id.title) as TextView
        descriptionText = view.findViewById(R.id.description) as TextView
        btnImageUpload = view.findViewById(R.id.btnImageUpload) as ImageButton
        days = view.findViewById(R.id.reservationLayout).find(R.id.days)
        days.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val newFragment = DatePickerDialogFragment(days, activity.baseContext);
                newFragment.show(fragmentManager, "DatePicker");
            }
        });


        postingDetails = view.findViewById(R.id.postingDetails) as TextView
        postingDetails.enabled = false

        wantitgonebyText = view.findViewById(R.id.wantitgonebyText) as TextView
        wantitgonebyText.enabled = false

//        btnInactivate = view.findViewById(R.id.btnInactivate) as Button

        offerSwitch.isChecked = true
        switchStatus.text = getString(R.string.give)
        offerSwitch.setOnCheckedChangeListener{compoundButton, isChecked ->
            if(isChecked) {
                switchStatus.text = getString(R.string.give)
                currentOfferType = GIVE
                availabilitySwitch.enabled = true
                wantitgonebyText.text = "Claim before "
            }
            else{
                switchStatus.text = getString(R.string.need)
                currentOfferType = NEED
                availabilitySwitch.enabled = false
                available = null
                wantitgonebyText.text = "Need by "
            }
//            override fun onCheckedChanged(button: CompoundButton, isChecked: Boolean){
        }

        availabilitySwitch.isChecked = true
        availabilitySwitchstatus.text = getString(R.string.title_available)
        //listener called only for GIVE Offer
        availabilitySwitch.setOnCheckedChangeListener{compoundButton, isChecked ->
            if(isChecked) {
                availabilitySwitchstatus.text = getString(R.string.title_available)
                available = true
            }
            else{
                availabilitySwitchstatus.text = getString(R.string.title_unavailable)
                available = false
            }
//            override fun onCheckedChanged(button: CompoundButton, isChecked: Boolean){
        }

        //show this button only for edit item
        btnImageUpload.onClick { v ->
            Log.d(TAG, "image upload clicked")
            val blnValidations = validateFields()
            Log.d(TAG,"validation is " + blnValidations.toString())
            if(blnValidations == true) {
                // save any edits and then open images screen after the edit
                    postOrput(true)
            }
            else{
                //no other edits.open images screen for photo edits
                val intent = Intent(activity, ImageUploadActivity::class.java)
                intent.putExtra("item", item)
                intent.putParcelableArrayListExtra("images", images)
                startActivityForResult(intent,500)
            }
        }

        if (item != null) {
            //existing item
            Log.d(TAG, item!!.image!!.count().toString())
            if (item?.image!!.count() == 0) {
                images = ArrayList<ImageModel?>()
            } else {
                initialImageCnt = item!!.image!!.count()
                images = item!!.image!!
                seq = images.size
            }
            //images = itemUser!!.item?.image ?: ArrayList<ImageModel?>()
            titleText.text = item?.title ?: null
            descriptionText.text = item?.description ?: null

            if (item?.reservationEndDate != null) {
                days!!.setText(item?.reservationEndDate)
            }

            latitude = item?.latitude
            longitude = item?.longitude

            //val df = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
            //insertedOrUpdatedAtText.text =  df.format(itemUser!!.item?.updated_at?.toString()) ?: null
            val userName = user!!.name ?: null
            var postedDate = item?.updated_at?.toString() ?: null
            postedDate = formatDate(postedDate)
            if (userName != null && postedDate != null) {
                postingDetails.text = "Posted by $userName on $postedDate"
            }
            //set the current item offer status
            if (item?.offerType == GIVE) {
                offerSwitch.isChecked
                switchStatus.text = getText(R.string.give)
                currentOfferType = GIVE
            }
            else{
                offerSwitch.isChecked = false
                switchStatus.text = getText(R.string.need)
                currentOfferType = NEED
                availabilitySwitch.enabled = false
                availabilitySwitchstatus.text = "Reserved/Available"
            }
            //set the current item availability switch status
            if (item?.available != null) { //only for offerswitch give
                if (item?.available == true) {
                    availabilitySwitch.isChecked
                    availabilitySwitchstatus.text = getText(R.string.title_available)
                    available = true
                } else {
                    availabilitySwitch.isChecked = false
                    availabilitySwitchstatus.text = getText(R.string.title_unavailable)
                    available = false
                }
            }
            else {//available is null
                availabilitySwitch.enabled = false
                availabilitySwitchstatus.text = "Reserved/Available"
            }

//                    userIdText.text = itemUser!!.user!!.name ?: null
        } else {
            //New item
            images = ArrayList<ImageModel?>()
            titleText.text = null
            descriptionText.text = null
            //TODO: update with current user details.Need to convert timestamps on server to UTC ans pass them from client
            postingDetails.text = null
            //default values for the item
            currentOfferType = GIVE
            wantitgonebyText.text = "Claim before "
            available = true
        }

        carouselView?.pageCount = images!!.size
        carouselView?.setCurrentItem(0)
        carouselView?.setImageListener(imageListener)

        //disable edit mode and hide imagebuttons
        decideMode(mEditable)

        return view
    }


    private fun formatDate(postedDate: String?): String? {
        if (postedDate != null) {
//            Log.d(TAG, " raw updated date" + postedDate)
            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val formattedDate = SimpleDateFormat("dd.MM.yyyy hh:mm")
            try {
                val d = dateFormat.parse(postedDate)
                System.out.println("DATE" + d);
                return formattedDate.format(d)
            } catch(e: Exception) {
                System.out.println("Excep" + e.message);
            }
        }
        return null
    }

    private fun decideMode(editable: Boolean) {
        if (editable) enableEditMode()
            else disableEditMode()
    }

    private fun enableEditMode() {
        activity.title = getText(R.string.title_item_detail_edit)
        titleText.enabled = true
        descriptionText.enabled = true
        days.enabled = true
//        btnImageCapture.visibility = View.VISIBLE
//        btnOpenGallery.visibility = View.VISIBLE
        offerSwitch.visibility = View.VISIBLE
        //image upload button only available for edit item
        if(mAddition == false && mEditable) {
            btnImageUpload.visibility = View.VISIBLE
            carouselView!!.visibility= View.VISIBLE
        }
        else {
            carouselView!!.visibility= View.GONE
            btnImageUpload.visibility = View.GONE
        }

        mapView.isClickable = true
        titleText.addTextChangedListener(object : TextValidator(titleText) {
            override fun validate(textView: TextView, text: String) {
                if (titleText.length() == 0) {
                    titleText.setError("Title is required")
                }
            }
        })

        descriptionText.addTextChangedListener(object : TextValidator(descriptionText) {
            override fun validate(textView: TextView, text: String) {
                if (descriptionText.text.length == 0) {
                    descriptionText.setError("Description is required")
                }
                if (descriptionText.text.length >= 100) {
                    descriptionText.setError("Maximum of 100 characters is allowed")
                }
            }
        })

    }

    private fun disableEditMode() {
        activity.title = getText(R.string.title_item_detail_view)
        titleText.enabled = false
        descriptionText.enabled = false
        days.enabled = false

        availabilitySwitchstatus.visibility = View.GONE
        switchStatus.visibility= View.GONE

        offerSwitch.visibility = View.GONE
        availabilitySwitch.visibility = View.GONE
        mapView.isClickable = false
        btnImageUpload.visibility = View.GONE
    }


    //***************BACK PRESS ON FRAGMENT**********************************
    override fun doBack() {
        if (validateFields()) {
            val bundle = Bundle()
            bundle.putString("message", resources.getText(R.string.unsaved_changes).toString())
            bundle.putInt("resultCode", HttpHandler.NO_UPDATE)
            val newFragment = AlertDialogFragment.newInstance(bundle)
            newFragment.show(fragmentManager, "AlertDialog")
        }
        else{
            activity.setResult(HttpHandler.NO_UPDATE)
            activity.finish()
        }
    }
    //*******************************************************************



    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(mdetailMenu, menu);
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        if (menuItem.title == resources.getString(R.string.action_message)) {
            Log.d(TAG, "message clicked")
//            val bundle = Bundle()
//            bundle.putE("itemUser", itemUser)
            val intent = Intent(activity, MessageActivity::class.java)
            intent.putExtra("item", item)
            intent.putExtra("user", user)
            startActivityForResult(intent, HttpHandler.MESSAGE_RESULT_CODE)
            return true
        }

        if(menuItem.title == resources.getString(R.string.action_forward)){ //forward present only for adding item screen
            //only go forward to adding images after saving item
            if (validateFields()){
                //opens images upload screen after posting item successfully
                postOrput(true)
            }
        }

        if (menuItem.title == resources.getString(R.string.action_done)) {
            Log.d(TAG, "done clicked during edit")

            val inputManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

            Log.d(TAG, "latitude is " + latitude)
            if (validateFields()){
                postOrput(false)
            }
            else {
                val bundle = Bundle()
                bundle.putString("message", resources.getText(R.string.no_changes).toString())
                bundle.putInt("resultCode", HttpHandler.NO_UPDATE)
                val newFragment = AlertDialogFragment.newInstance(bundle)
                newFragment.show(fragmentManager, "AlertDialog")
            }
            return true
            //initiate Put based on changes made
        }
        if (menuItem.title == resources.getString(R.string.action_delete)) {
            Log.d(TAG, "delete clicked")
            delete()
        }
        return true
    }

    fun validateFields(): Boolean {
        //if we are on the view mode only, no need to check for validations
        if(mEditable == false){
            return false
        }
        // Log.d("aR", itemUser!!.item!!.image.count().toString())
        if (item != null) {
            //editing an item
            if (item!!.title == titleText.text.toString()
                    && item!!.description == descriptionText.text.toString()
                    && (days.text.toString() != "" && (item!!.reservationEndDate == days.text.toString()) || days.text.toString() == "")
                    && (item!!.available == available)
                    && (latitude != null && (item!!.latitude == latitude) || latitude == null)
                    && (longitude != null && (item!!.longitude == longitude) || longitude == null)
                    && (item!!.offerType == currentOfferType)){
//                && initialImageCnt == images.count()
                //lat and long are null only for records that dont have location info in table(before lat long implementation)..henceforth its mandatory
                Log.d(TAG, "no changes occured ")
                return false
            }
        }

        if (titleText.text.length == 0 || titleText.error != null) {
            titleText.setError("Title is required")
            return false
        }
        if (descriptionText.text.length == 0 || descriptionText.error != null) {
            descriptionText.setError("Description is required")
            return false
        }

        if (descriptionText.text.length >= 100) {
            descriptionText.setError("Maximum of 100 characters is allowed")
            return false
        }
        return true
    }

    //********** IMAGE PROCESSING FUNCTIONS *********************** //
    val imageListener = com.synnapps.carouselview.ImageListener { position: Int, imageView: ImageView ->
        var cachedImage: String? = null
        var file: File? = null

        var imageUrl: String? = null
        if (images[position]!!.url != null) imageUrl = images[position]!!.url
        else imageUrl = images[position]!!.fileNameWithlocalPath
        file = File(context.getExternalFilesDir(null).absolutePath, File(imageUrl)!!.absoluteFile.name)
        val target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
                // Bitmap is loaded, use image here
                imageView.setImageBitmap(bitmap)
                targets.remove(this)
                if (!file!!.exists()) {
                    Log.d(TAG, "file does not exist on phone")
                    //TODO: need a naming pattern for images and backup
                    try {
                        file?.createNewFile()
                        val ostream = FileOutputStream(file)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream)
                        ostream.flush()
                        ostream.close()
                    } catch (e: IOException) {
                        Log.e("IOException", e.message)
                    }
                }
            }

            override fun onBitmapFailed(errorDrawable: Drawable) {
                targets.remove(this);
            }

            override fun onPrepareLoad(placeHolderDrawable: Drawable) {
                Log.i("load", "first " + images[position]!!.url);
            }
        }
        targets.add(target);

        imageView!!.setOnClickListener(View.OnClickListener {
            val bundle = Bundle()
            val imageLocationsList = arrayListOf<String?>()
            for (i in images) {
                imageLocationsList!!.add(i!!.fileNameWithlocalPath)
            }
            bundle.putStringArrayList("imageLocations", imageLocationsList)
            bundle.putInt("currentPos", position)

//            bundle.putInt("currentPosition", carouselView.)
            val intent = Intent(activity, ImageFullcreenActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        })

        if (file != null && file!!.exists()) {
            Log.d(TAG, "loading image from phone" + file.name)
            picasso.load(file).
                    placeholder(R.drawable.thumbnail_launcher).
                    into(target)
        } else {
            if (images[position]!!.url != null) {
                Log.d(TAG, "loading image from network")
                picasso.load(images[position]!!.url).
                        placeholder(R.drawable.thumbnail_launcher).
                        into(target)
            }
        }
        //carouselView?.setCurrentItem(position)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == HttpHandler.BACK_FROM_IMAGE_UPLOADS_NO_SAVE){
            Log.d(TAG, "do nothing after back on upload image")
        }
        if(resultCode == HttpHandler.MESSAGE_RESULT_CODE) {
            Log.d(TAG, "do nothing after adding message")
        }

        if(resultCode == HttpHandler.BACK_FROM_IMAGE_UPLOADS_SAVE) {
            activity.setResult(HttpHandler.UPDATE)
            activity.finish()
        }
    }
    //******************************************************************

    //****************************NETWORK CALLS *************************

    private fun delete() {
        headerProgress!!.visibility = View.VISIBLE
        activity.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        async() {
            var result = ItemDataManager.delete(ItemDataManager.ALL_ITEMS , item!!.id!!, token)
            result!!.success {
                Log.d(TAG, "item deleted successfully")
            }
            result!!.failure {
                activity.runOnUiThread {
                    //TODO: read changeset to read error
                    Log.d(TAG, "in delete error" + result!!.component2()!!.message.toString())
                    headerProgress!!.visibility = View.GONE
                    activity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                }
            }
                uiThread {
                    activity.setResult(HttpHandler.UPDATE)
                    activity.finish()
                    Log.d(TAG, "run on async ui thread")
                }
        }
    }

    fun postOrput(openImageUpload: Boolean) {
        headerProgress!!.visibility = View.VISIBLE

        activity.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        val format = SimpleDateFormat("MM/dd/yyyy")

        val reservationEndDate = days.text.toString()
        Log.d(TAG, "reservation end date is " + reservationEndDate)
        //        async() {
        if (item == null) {

            //post request
            reqObject = ItemModel(null, titleText.text.toString(), descriptionText.text.toString(), null, reservationEndDate, latitude, longitude, null, available, currentOfferType, null)
            //call http post
//            val images = reqObject!!.image
            async() {
//                val result = HttpHandler.post(context, postUrl, reqObject, token)
//                val result = HttpHandler.post(postUrl, reqObject, token)
                //val result = HttpHandler.post(postUrl, reqObject, token)
                val result = UserDataManager.postItem(UserDataManager.ALL_USERS,reqObject,user!!.id!!,token)
                result!!.success {
                    Log.d(TAG, "Item posted successfully")
//                    uploadImages(images, result)
                    if(openImageUpload) {
                        val responseItem = result!!.component1() as ItemModel
                        if (responseItem != null) {
                            // show page to upload images
                            val intent = Intent(activity, ImageUploadActivity::class.java)
                            intent.putExtra("item", responseItem)
                            intent.putParcelableArrayListExtra("images", images)
                            startActivityForResult(intent, 500)
                        }
                    }
                }//end result success
                result!!.failure {
                    activity.runOnUiThread {
                        Log.d(TAG, "in post error" + result!!.component2()!!.message.toString())
                        headerProgress!!.visibility = View.GONE
                        activity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    }
                }
                uiThread {
                    headerProgress!!.visibility = View.GONE
                    activity.setResult(HttpHandler.UPDATE)
                    activity.finish()
                    Log.d(TAG, "run on async ui thread")

                }
            }

        } else {
            //ensure available is null if offerType changed from give to need
            if(currentOfferType == NEED){
                available = null
            }
            val putUrl = ItemDataManager.ALL_ITEMS + "/" + item!!.id
            reqObject = ItemModel(item!!.id, titleText.text.toString(), descriptionText.text.toString(), null, reservationEndDate, latitude, longitude, null, available, currentOfferType, null)
            async() {
//                val result = HttpHandler.put(putUrl, reqObject, token)
                val result = ItemDataManager.put(putUrl, reqObject, token)
                result!!.success {
                    Log.d(TAG, "Item updated successfully")
                    if(openImageUpload){
                        Log.d(TAG, "open the images screen")
                        val responseItem = result!!.component1() as ItemModel
                        if(responseItem !=null) {
                            // show page to upload images
                            val intent = Intent(activity, ImageUploadActivity::class.java)
                            intent.putExtra("item", responseItem)
                            intent.putParcelableArrayListExtra("images", images)
                            startActivityForResult(intent, 500)
                        }
                    }
//                    activity.runOnUiThread {
                    Log.d(TAG, "do nothing else.only item edited. click on images button to do any updates")
//                    val responseItem = result!!.component1() as ItemModel
//                    if(responseItem !=null){
//                        // show page to upload images
//                        val intent = Intent(activity, ImageUploadActivity::class.java)
//                        intent.putParcelableArrayListExtra("images", images)
//                        startActivityForResult(intent,500)
//                    }
//                    uploadImages(images, result)
//                    }
                }
                result!!.failure {
                    activity.runOnUiThread {
                        Log.d(TAG, "in post error" + result!!.component2()!!.message.toString())
                        headerProgress!!.visibility = View.GONE
                        activity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    }
                }
                uiThread {
                    headerProgress!!.visibility = View.GONE
                    activity.setResult(HttpHandler.UPDATE)
                    activity.finish()
                    Log.d(TAG, "run on async ui thread")
                }
            }
        }
    }

    //******************************************************************

    //*************Map and location change *******************

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
                .setInterval(10 * 1000)
                .setFastestInterval(1 * 1000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setSmallestDisplacement(150f)
    }

    @Synchronized protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(activity).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build()
    }

    private fun checkPlayServices(): Boolean {

        val googleApi = GoogleApiAvailability.getInstance()
        val result = googleApi.isGooglePlayServicesAvailable(activity)
        if (result != ConnectionResult.SUCCESS) {
            if (googleApi.isUserResolvableError(result)) {
                googleApi.getErrorDialog(activity, result, PLAY_SERVICES_RESOLUTION_REQUEST).show()
                activity.finish()
                return false
            }
        }
        return true
    }


    override fun onActivityCreated(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState);
        if(checkPlayServices()){
            mapsSupported= true
            Nammu.init(activity)
            mGoogleApiHelper = App.googleApiHelper
            Log.d(TAG, "status of google api helper :" + mGoogleApiHelper.isConnected)
            MapsInitializer.initialize(activity);

            if (mapView != null) {
                mapView.onCreate(savedInstanceState);
            }
            initializeMap()

        }
    }


    override fun onSaveInstanceState( savedInstanceState : Bundle) {
        super.onSaveInstanceState(savedInstanceState);
        mapView.onSaveInstanceState(savedInstanceState);
    }

    private fun checkLocationPermission(){
        val permissionCheck = Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)
        if(permissionCheck == true){
//            Log.d(TAG,"Loction availability" + FusedLocationApi.getLocationAvailability(mGoogleApiClient).isLocationAvailable)
            if(map!= null) {
                map!!.isMyLocationEnabled = true
                map!!.uiSettings.isMyLocationButtonEnabled = true
            }

            Log.d(TAG,"ItemDetailFragment location when permission is present:" + mGoogleApiHelper.isConnected)
            if(mGoogleApiHelper.isConnected) {
                location = mGoogleApiHelper.currentLocation
                update(location)
            }
        }
        else {
                Log.d(TAG, "if user had clicked never ask permission or first time or settings location is turned off")
                Nammu.askForPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION,permissionLocationCallback)
            }
        }

    internal val permissionLocationCallback: PermissionCallback = object : PermissionCallback {
        override fun permissionGranted() {
            if(map != null) {
                map!!.isMyLocationEnabled = true
                map!!.uiSettings.isMyLocationButtonEnabled = true
            }
            Log.d(TAG,"ItemDetailFragment location when permission is granted:" + mGoogleApiHelper.isConnected)
            if(mGoogleApiHelper.isConnected){
                location = mGoogleApiHelper.currentLocation
                update(location)
                }

           // Toast.makeText(activity, latitude.toString() + " WORKS " + longitude + "", Toast.LENGTH_LONG).show();
        }
        override fun permissionRefused() {
            //Toast.makeText(activity,"in nammu callback for refused", Toast.LENGTH_SHORT).show()
            if(map != null) {
                map!!.isMyLocationEnabled = false
                map!!.uiSettings.isMyLocationButtonEnabled = false
            }
            //            boolean hasAccess = Tools.accessContacts(MainActivity.this);
            //            Toast.makeText(MainActivity.this, "Access granted = " + hasAccess, Toast.LENGTH_SHORT).show();
        }
    }

    private fun initializeMap() {
        if (map == null) {
            mapView = activity.findViewById(R.id.map) as MapView
            mapView.getMapAsync { p0 ->
                Log.d(TAG, "map ready")
                map = p0
                if(mEditable) {
                    Log.d(TAG,"ItemDetailFragment location when permission is present:" + mGoogleApiHelper.isConnected)
                    if (mGoogleApiHelper.isConnected) {
                        Log.d(TAG, "only first time")
                        checkLocationPermission()
                    }
                }

                //draw marker for the item location when the map is ready
                    if(latitude!= null && longitude!= null) {
                        map!!.addCircle(CircleOptions().center(LatLng(latitude!!.toDouble(), longitude!!.toDouble())).radius(200.0).strokeColor(Color.RED))
                        map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude!!.toDouble(), longitude!!.toDouble()), 14.0f))
                    }

                map!!.setOnMyLocationButtonClickListener{
                    if (location != null) {
                        Log.d(TAG, "animate to my location")
//                        latitude = location!!.latitude.toFloat()
//                        longitude = location!!.longitude.toFloat()
//                        map!!.addCircle(CircleOptions().center(LatLng(location!!.latitude, location!!.longitude)).radius(200.0).strokeColor(Color.RED))
                        map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location!!.latitude, location!!.longitude), 14.0f))
                        if(item == null){
                            //new item
                            map!!.addCircle(CircleOptions().center(LatLng(location!!.latitude, location!!.longitude)).radius(200.0).strokeColor(Color.RED))
                            latitude = location!!.latitude.toFloat()
                            longitude = location!!.longitude.toFloat()
                            //set lat lng of the item to these values
                        }
                    }
                    true
                }

                    map!!.setOnMapClickListener(GoogleMap.OnMapClickListener { latLng ->
                    map!!.clear()
                    map!!.addCircle(CircleOptions().center(LatLng(latLng.latitude, latLng.longitude)).radius(200.0).strokeColor(Color.RED))
                    //assign new lat long
                    latitude = latLng!!.latitude.toFloat()
                    longitude = latLng!!.longitude.toFloat()
                    // Animating to the touched position
                    map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f))
                })
            }
        }       //setup markers etc...
    }



    //*******Map interface overrides**********************************************************************

    override fun onConnected(p0: Bundle?) {
        Log.d(TAG, "m i in connected")
//            enableMyLocation()
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.d(TAG, "m i in suspended")
        if(mEditable) {
//            if (mGoogleApiClient != null && mapsSupported) {
//                mGoogleApiClient!!.connect()
//            }
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.d(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + p0.getErrorCode())
    }


    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {

//        Toast.makeText(activity, "request code" + requestCode, Toast.LENGTH_SHORT).show()
        Nammu.onRequestPermissionsResult(requestCode,permissions,grantResults)
    }
    //***********************************************************************************************
    //*******Fragment method overrides***************************************************************
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context as OnFragmentInteractionListener?
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onDestroy() {
        mapView.onDestroy()
        super.onDestroy()
    }

    override fun onStart(){
        super.onStart()
    }

    override fun onPause(){
//        stopLocationUpdates()
        mapView!!.onPause()
        if(mEditable) {
//            mGoogleApiHelper.disconnect()
        }
//        mGoogleApiHelper.disconnect()
        super.onPause()
    }

    override fun onLowMemory(){
        super.onLowMemory()
        mapView!!.onLowMemory()
    }

    override fun onResume(){
        super.onResume()
        mapView!!.onResume()
        initializeMap()
            if (mEditable) {
                Nammu.permissionCompare(object: PermissionListener {
                    override fun permissionsChanged(permissionRevoke: String)
                    {
                        //Toast.makeText(activity, "Access granted or removed = " + permissionRevoke, Toast.LENGTH_SHORT).show()
                    }
                    override  fun permissionsGranted(permissionGranted: String){
                        Log.d(TAG, "permission granted change")
                        if(map != null) {
                            map!!.isMyLocationEnabled = true
                            map!!.uiSettings.isMyLocationButtonEnabled = true
                        }
                        Log.d(TAG, "permission granted " + mGoogleApiHelper.isConnected)
                        if(mGoogleApiHelper.isConnected){
                            location = mGoogleApiHelper.currentLocation
                            update(location)

//                    Toast.makeText(activity, currentLatitude.toString() + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
                        }
                    }
                    override fun permissionsRemoved(permissionRemoved: String){
                        if(map != null) {
                            map!!.isMyLocationEnabled = false
                            map!!.uiSettings.isMyLocationButtonEnabled = false
                        }
                    }
                })
            }
    }

    fun update(currentLocation: Location? ){
        Log.d(TAG,"in update")
        if(map!=null) {
            if(item != null) {
                // in edit item
                if(item!!.latitude !=null && item!!.longitude != null) {
                    //show the lat lng already set for this item with red circle around
                    map!!.addCircle(CircleOptions().center(LatLng(item!!.latitude!!.toDouble(), item!!.longitude!!.toDouble())).radius(200.0).strokeColor(Color.RED))
                    map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(item!!.latitude!!.toDouble(), item!!.longitude!!.toDouble()), 14.0f))
                }
                else{
                    Log.d(TAG, "item exists without location-- may b no permission or map problem")
                    latitude = null
                    longitude = null
                }
            }else{
                //new item - animate camera to current location and add a red circle around
                if(currentLocation!= null) {
                    map!!.addCircle(CircleOptions().center(LatLng(currentLocation!!.latitude!!.toDouble(), currentLocation!!.longitude!!.toDouble())).radius(200.0).strokeColor(Color.RED))
                    map!!.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(currentLocation!!.latitude!!.toDouble(), currentLocation!!.longitude!!.toDouble()), 14.0f))
                    latitude = currentLocation!!.latitude!!.toFloat()
                    longitude = currentLocation!!.longitude!!.toFloat()
                }
                else{
                    Log.d(TAG, "connection returned no location")
                }
            }
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        /**
         * Create a new instance of DetailsFragment, initialized to
         * show the text at 'index'.
         */

        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        private val ARG_COLUMN_COUNT = "column-count"
        private val ITEM = "item"
        private val USER = "user"
        private val EDITABLE = "editable"
        private val ADDITION = "addition"
        private val MENU = "menu"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment ItemDetailFragment.
         */
        // TODO: Rename and change types and number of parameters

        fun newInstance(columnCount: Int, item: ItemModel?, user: UserModel?, detailMenu: Int, editable: Boolean, addition: Boolean): ItemDetailFragment {
            val fragment = ItemDetailFragment()
            val args = Bundle()
            args.putInt(ARG_COLUMN_COUNT, columnCount)
            args.putBoolean(EDITABLE, editable)
            args.putBoolean(ADDITION, addition)
            args.putParcelable(ITEM, item)
            args.putParcelable(USER, user)
            args.putInt(MENU, detailMenu)
            //args.putString(ARG_PARAM2, param2);
            fragment.arguments = args
            return fragment
        }
    }
}








