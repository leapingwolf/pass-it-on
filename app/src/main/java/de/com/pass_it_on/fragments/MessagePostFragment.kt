package de.com.pass_it_on.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import de.com.pass_it_on.R
import de.com.pass_it_on.activities.LoginActivity
import de.com.pass_it_on.models.ConversationModel
import de.com.pass_it_on.models.MessageModel
import de.com.pass_it_on.models.MessageResponse
import de.com.pass_it_on.models.UserModel
import de.com.pass_it_on.network.ConversationDataManager
import de.com.pass_it_on.network.SessionManager
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [MessagePostFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [MessagePostFragment.newInstance] factory method to
 * create an instance of this fragment.

 */
class MessagePostFragment : Fragment() {
    final val TAG : String = "MessagePostFragment"
    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    var conversation: ConversationModel? = null
    var mConversationId: Int? = null
    var mEnquirerId: Int? = null
    var mItemOwnerId: Int? = null
    var mItemId: Int? = null
    private var mListener: OnFragmentInteractionListener? = null
    lateinit var currentUser: UserModel
    private var session: SessionManager? = null
    lateinit var token: String
    private var mCallBack: MessagePassingInterface? = null
    lateinit var mMessageResponseArray: Array<MessageResponse>

    interface MessagePassingInterface {
        fun passMessagePostResultSet(messageResponseArray: ArrayList<MessageResponse?>)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance= true
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var blnValidated: Boolean = true
        var message: MessageModel? = null

        session = SessionManager(activity.applicationContext)
        if (session!!.isLoggedIn) {
            val user = session!!.userDetails
            val id = user[SessionManager.KEY_ID]!!.toInt()
            val name = user[SessionManager.KEY_NAME]!!.toString()
            val email = user[SessionManager.KEY_EMAIL]!!.toString()
            token = user[SessionManager.KEY_TOKEN]!!.toString()
            currentUser = UserModel(id, name, email, false)
        } else {
            val toast = Toast.makeText(context, R.string.invalid_user_session, Toast.LENGTH_SHORT)
            toast.show()
            val intent = Intent(activity.baseContext, LoginActivity::class.java)
            startActivity(intent)
            activity.finish()
        }

        val view = inflater!!.inflate(R.layout.fragment_message_post, container, false)
        val btnSend = view.findViewById(R.id.btnSend) as ImageButton?
        val messageText = view.findViewById(R.id.message) as EditText?
//        val headerProgress = view.findViewById(R.id.pbHeaderProgress) as ProgressBar?
//        headerProgress!!.visibility= View.GONE
            btnSend!!.setOnClickListener {
                val arrMessageModel = ArrayList<MessageModel?>()
//                Toast.makeText(activity,mConversationId.toString(), Toast.LENGTH_SHORT).show()

                if (messageText!!.text.length == 0) {
                    messageText.error = "Message is required"
                    blnValidated = false
                }
                    else{ blnValidated = true
                }
                if(blnValidated) {
                    val inputManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputManager.hideSoftInputFromWindow(messageText!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
                    if (mConversationId == null) {
                        //no conversation exists. Conversation always initiated by enquirer.. so sender is current user and receiver is item owner
                        message = MessageModel(currentUser!!.id, null, mItemOwnerId, null, messageText!!.text.toString(), null)
                        arrMessageModel.add(message)
                        conversation = ConversationModel(null, currentUser!!.id, mItemOwnerId, mItemId, null, arrMessageModel)
                    } else {// conversation exists
                        if (currentUser!!.id == mItemOwnerId) {
                            //message sender is current user(also item owner) which means receiver is the enquirer
                            message = MessageModel(currentUser!!.id, null, mEnquirerId, mConversationId, messageText!!.text.toString(), null)
                            arrMessageModel.add(message)
                            conversation = ConversationModel(null, currentUser!!.id, mEnquirerId, mItemId, null, arrMessageModel)
                        } else {
                            //message sender is current user(not  item owner) which means receiver is the itemOwner
                            message = MessageModel(currentUser!!.id, null, mItemOwnerId, mConversationId, messageText!!.text.toString(), null)
                            arrMessageModel.add(message)
                            conversation = ConversationModel(null, currentUser!!.id, mItemOwnerId, mItemId, null, arrMessageModel)
                        }
                    }

                    //TODO: pass conver id

//                    headerProgress!!.visibility = View.VISIBLE
//                    activity.pbHeaderProgress!!.visibility = View.VISIBLE
                    activity.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    async() {
                        //server check to decide conversation upsert logic
//                        val result = HttpHandler.put(HttpHandler.CONVERSATIONS, conversation, token)
                        val result = ConversationDataManager.put(ConversationDataManager.CONVERSATIONS, conversation, token)
                        result!!.success {
                            Log.d(TAG, "conversation inserted")
                            activity.runOnUiThread {
                                val mArrayList = ArrayList<MessageResponse?>()
                                mArrayList.addAll(result!!.component1()!!.toList())
                                mCallBack!!.passMessagePostResultSet(mArrayList)
                                messageText.text = null
//                                activity.pbHeaderProgress!!.visibility = View.GONE
//                                headerProgress!!.visibility = View.GONE
                                activity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                            }
                        }
                        result!!.failure {
                            activity.runOnUiThread {
                                Log.d(TAG, "in post error" + result!!.component2()!!.message.toString())
//                                headerProgress!!.visibility = View.GONE
                                activity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                            }
                        }
                        uiThread {
                            Log.d(TAG, "done")
//                            setResult(100)
//                            finish()
                        }
                    }
                }
            }
        return view

    }

    fun getConversationData(conversationId: Int?, enquirerId: Int?, itemOwnerId: Int?, itemId: Int?){
        mConversationId = conversationId
        mEnquirerId = enquirerId
        mItemOwnerId = itemOwnerId
        mItemId= itemId
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MessagePassingInterface) {
            mCallBack = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement testInterface")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"
        private val ITEM_USER = "item_user"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.

         * @param param1 Parameter 1.
         * *
         * @param param2 Parameter 2.
         * *
         * @return A new instance of fragment MessagePostFragment.
         */

        fun newInstance(itemId: Int?, itemOwnerId: Int?, enquirerId: Int?): MessagePostFragment {
            val fragment = MessagePostFragment()
            val args = Bundle()
//            args.putParcelable(MessagePostFragment.ITEM_USER, itemUser)
            fragment.arguments = args
            return fragment
        }

        // TODO: Rename and change types and number of parameters
        fun newInstance(): MessagePostFragment {
            val fragment = MessagePostFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
