package de.com.pass_it_on.activities

import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import de.com.pass_it_on.models.ConversationResponse
import de.com.pass_it_on.models.ItemModel
import de.com.pass_it_on.models.ItemUserModel

/**
 * Created by akshatashan on 4/19/16.
 */
interface HttpInterface {
//    fun getItems(pageNumber: Int, url: String, query: String?, orderBy: Int): Result<Any, FuelError>?
    fun getMyItems(pageNumber: Int, url: String, query: String?, orderBy: Int): Result<Array<ItemModel>, FuelError>?
    fun getAllItems(url: String, pageNumber: Int,  query: String?,latitude: String?, longitude: String?, orderBy: Int): Result<Array<ItemUserModel>, FuelError>?
    fun getConversations(url: String, pageNumber: Int,  orderBy: Int): Result<Array<ConversationResponse>, FuelError>?
    //    fun getItems(query: String?, url: String):Array<ItemUserModel>?
    companion object {
    }
}

