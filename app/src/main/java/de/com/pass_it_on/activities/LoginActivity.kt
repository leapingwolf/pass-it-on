package de.com.pass_it_on.activities

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import de.com.pass_it_on.R
import de.com.pass_it_on.models.RegistrationResponse
import de.com.pass_it_on.models.RegistrationUser
import de.com.pass_it_on.models.UserModel
import de.com.pass_it_on.network.HttpHandler
import de.com.pass_it_on.network.SessionManager
import de.com.pass_it_on.network.UserDataManager
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread

class LoginActivity : AppCompatActivity() {
    final val TAG : String = "LoginActivity"
    ///**
    // * Author: Ravi Tamada
    // * URL: www.androidhive.info
    // * twitter: http://twitter.com/ravitamada
    // */
    //
    //
    //
    //
    //    import info.androidhive.loginandregistration.R;
    //    import info.androidhive.loginandregistration.app.AppConfig;
    //    import info.androidhive.loginandregistration.app.AppController;
    //    import info.androidhive.loginandregistration.helper.SQLiteHandler;
    //    import info.androidhive.loginandregistration.helper.SessionManager;
    //
    //    public class LoginActivity extends Activity {
    //        private static final String TAG = RegisterActivity.class.getSimpleName();
    private var btnLogin: Button? = null
    private var btnRegister: Button? = null
    private var inputEmail: EditText? = null
    private var inputPassword: EditText? = null
    private var pDialog: ProgressDialog? = null
    private var session: SessionManager? = null
//    private var btnFacebookLogin: LoginButton? = null
    private var btnFacebookLogin: Button? = null
    //        private SQLiteHandler db;

    public override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_login)
        inputEmail = findViewById(R.id.email) as EditText?
        inputPassword = findViewById(R.id.password) as EditText?
        btnLogin = findViewById(R.id.btnLogin) as Button?
        btnRegister = findViewById(R.id.btnRegister) as Button?
//        btnFacebookLogin = findViewById(R.id.btnFacebookLogin) as LoginButton?
        btnFacebookLogin = findViewById(R.id.btnFacebookLogin) as Button?

        btnFacebookLogin!!.setOnClickListener({

            val intent = Intent(this@LoginActivity, FbLoginActivity::class.java)
            startActivity(intent)
            finish()
        })

        // Progress dialog
        pDialog = ProgressDialog(this)
        pDialog!!.setCancelable(false)


        // Session manager
        session = SessionManager(applicationContext)

        // Check if user is already logged in or not
        if (session!!.isLoggedIn) {
            // User is already logged in. Take him to main activity
            val intent = Intent(this@LoginActivity, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Login button Click Event
        btnLogin!!.setOnClickListener {
            val email = inputEmail!!.text.toString().trim { it <= ' ' }
            val password = inputPassword!!.text.toString().trim { it <= ' ' }

            val inputManager = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(inputPassword!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)

            // Check for empty data in the form
            if (!email.isEmpty() && !password.isEmpty()) {
                // login user
                checkLogin(email, password)
            } else {
                // Prompt user to enter credentials
                Toast.makeText(applicationContext,
                        "Please enter the credentials!", Toast.LENGTH_LONG).show()
            }
        }

        // Link to Register Screen
        btnRegister!!.setOnClickListener {
            val i = Intent(applicationContext,
                    RegisterActivity::class.java)
            startActivity(i)
            finish()
        }

    }


    /**
     * function to verify login details in mysql db
     */
    private fun checkLogin(email: String, password: String) {
        // Tag used to cancel the request
        pDialog!!.setMessage("Logging in ...")
        showDialog()
        try {
            async() {
//                val registrationUser = RegistrationUser("", "12345", "", "",  "akshatashan@gmail.com")
                val registrationUser = RegistrationUser("", password, "", "", email)
                Log.d(TAG, "sending request for " + email + "and" + password)
                val result = UserDataManager.post( UserDataManager.IDENTITY_CALLBACK, registrationUser)
                uiThread {
                    result!!.success {
                        val registrationResponse = result!!.component1() as RegistrationResponse
                        if (registrationResponse.error.length > 0) {
                            Log.d(TAG, "registration error" + registrationResponse.error)
                            var errorMessage = ""
                            val errors = HttpHandler.errors()
                            for (err: String in errors.keys) {
                                if (err == registrationResponse.error) {
                                    val toast = Toast.makeText(baseContext, errors.get(err).toString(), Toast.LENGTH_SHORT)
                                    toast.show()
                                }
                            }
                        } else {
                            val currentUser = registrationResponse.user as UserModel
                            if( registrationResponse.authorization !== null) {
//                                val fcmToken = FirebaseInstanceId.getInstance().token
                                session!!.createLoginSession(currentUser.id, currentUser.name!!, currentUser.email!!, registrationResponse.authorization)
                            }
                            Log.d(TAG, "values:" + currentUser!!.name + currentUser!!.email)
                        }

                    }
                    result!!.failure {
//                        runOnUiThread {
//                            Log.d(TAG, "in post error" + result!!.component2()!!.message.toString())
                            hideDialog()
                            val toast = Toast.makeText(baseContext, result!!.component2()!!.toString(), Toast.LENGTH_SHORT)
                            toast.show()
                    }
                }
                uiThread {
                    hideDialog()
                    session = SessionManager(applicationContext)
                    if(session!!.isLoggedIn) {
                        val intent = Intent(this@LoginActivity, HomeActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }
            }
        } catch (e: Exception) {
            Log.d(TAG, e.message.toString())
        }
    }

    private fun showDialog() {
        if (!pDialog!!.isShowing)
            pDialog!!.show()
    }

    private fun hideDialog() {
        if (pDialog!!.isShowing)
            pDialog!!.dismiss()
    }


    override fun onBackPressed() {
        moveTaskToBack(true);
    }

    override fun onDestroy(){
        Log.d(TAG, "Login destroyed")
        super.onDestroy()

    }
}
