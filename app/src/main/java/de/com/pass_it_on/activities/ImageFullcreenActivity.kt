package de.com.pass_it_on.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.widget.Button
import com.squareup.picasso.Picasso
import com.synnapps.carouselview.CarouselView
import de.com.pass_it_on.R
import uk.co.senab.photoview.PhotoViewAttacher
import java.io.File

class ImageFullcreenActivity : AppCompatActivity() {
    final val TAG : String = "ImageFullcreenActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_fullcreen)
        //        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //        setSupportActionBar(toolbar);

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val imgDisplay: CarouselView?
        val btnClose: Button

        imgDisplay = findViewById(R.id.carouselView) as CarouselView?

        //        btnClose = (Button) findViewById(R.id.btnClose);

        val extras = intent.extras
        val imageLocations = extras.getStringArrayList("imageLocations")
        val currentPos = extras.getInt("currentPos")
        imgDisplay!!.pageCount = imageLocations.size

        imgDisplay!!.setImageListener { position, imageView ->
            val mAttacher = PhotoViewAttacher(imageView)
            try {

                Picasso.with(this@ImageFullcreenActivity).load(File(imageLocations[position])).fit().centerInside().into(imageView)
                mAttacher.update()
            } catch (ex: Exception) {
                Log.d(TAG, ex.message)
            }
        }
        imgDisplay!!.setCurrentItem(currentPos)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                //NavUtils.navigateUpFromSameTask(this);
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

}
