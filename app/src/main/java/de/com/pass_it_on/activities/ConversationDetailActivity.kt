package de.com.pass_it_on.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.Toast
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import de.com.pass_it_on.R
import de.com.pass_it_on.fragments.MessageFragment
import de.com.pass_it_on.fragments.MessagePostFragment
import de.com.pass_it_on.models.ConversationResponse
import de.com.pass_it_on.models.MessageResponse
import de.com.pass_it_on.models.UserModel
import de.com.pass_it_on.network.ConversationDataManager
import de.com.pass_it_on.network.SessionManager
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import java.util.*

/**
 * Created by akshata on 30/08/16.
 */
class ConversationDetailActivity : AppCompatActivity(), MessagePostFragment.MessagePassingInterface {
    final val TAG : String = "ConversationDetailActivit"
    override fun passMessagePostResultSet(messageResponseArray: ArrayList<MessageResponse?>) {
        val messageFragment = supportFragmentManager.findFragmentById(R.id.fragment_container) as MessageFragment?
        messageFragment!!.update(messageResponseArray)
    }


    //        ActivityCompat.OnRequestPermissionsResultCallback {
//    private val callingActivity: Int = R.string.title_activity_item_detail
    var detailMenu: Int = 0
    private  var currentUser: UserModel? = null
    lateinit var token: String
    var conversationResponse: ConversationResponse? = null
    lateinit var mValues: ArrayList<MessageResponse?>
    private var headerProgress: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_conversation_detail)
        if (savedInstanceState == null) {
            val session = SessionManager(applicationContext)
            currentUser = session!!.checkLogin()
            if (currentUser == null) {
                val toast = Toast.makeText(this@ConversationDetailActivity, R.string.invalid_user_session, Toast.LENGTH_SHORT)
                toast.show()
                val intent = Intent(this@ConversationDetailActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }

            headerProgress = findViewById(R.id.pbHeaderProgress) as ProgressBar?
            headerProgress!!.visibility = View.GONE

            val extras = this.intent.extras
            conversationResponse = extras.getParcelable<ConversationResponse?>("conversationResponse")
            token = session!!.userDetails[SessionManager.KEY_TOKEN].toString()

            if(currentUser!!.id == conversationResponse!!.sender_id) {
                title = "Conversation with " + conversationResponse!!.receiverName
            }
            else{
                title = "Conversation with " + conversationResponse!!.senderName
            }
            // During initial setup, plug in the details fragment.
//            setContentView(R.layout.activity_conversation_detail)
            val messageFragment = MessageFragment.newInstance()
//            val messagePostFragment = MessagePostFragment.newInstance("a","b")
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, messageFragment).commit()
            asyncTask()

            val messagePostFragment = MessagePostFragment.newInstance()
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container_post, messagePostFragment).commit()

            //supportFragmentManager.beginTransaction().replace(R.id.fragment_container1, messagePostFragment).commit()
        }
    }

    fun asyncTask(){
        headerProgress!!.visibility = 1
        mValues = ArrayList<MessageResponse?>()
        async() {
            var result: Result<Array<MessageResponse>, FuelError>? = null
            if(conversationResponse != null){
//                result  = HttpHandler.getMessages(HttpHandler.API_URL + "/auth/conversations" + "/" + conversationResponse!!.conversation!!.id, token)
                result  = ConversationDataManager.get(ConversationDataManager.CONVERSATIONS, token, conversationResponse!!.conversation!!.id!!)
            }
            result!!.success {
                Log.d(TAG, "query result returned")
                val messageArray = result!!.component1()
                runOnUiThread {
                    if (messageArray != null) {
                        Log.d(TAG, "get messages for the conversation")
                        mValues.clear()
                        if (messageArray!!.count() > 0) {
                            //messaeArray will atleast have one record cos conversation already exisits
                            setConversationData()
                            mValues.addAll(messageArray.toList())
                        }
                    }
                }
            }

            result!!.failure {
                runOnUiThread {
                    Log.d(TAG, result!!.component2()!!.toString())
                    Log.d(TAG, "conversation not fetched")
                    headerProgress!!.visibility = View.GONE
                    val toast = Toast.makeText(this@ConversationDetailActivity, R.string.ConnectionError, Toast.LENGTH_SHORT)
                    toast.show()
                }
            }
            uiThread {
                headerProgress!!.visibility = View.GONE
                Log.d(TAG, "refresh view")
                val messageFragment = supportFragmentManager.findFragmentById(R.id.fragment_container) as MessageFragment?
                messageFragment!!.update(mValues)
            }
        }
    }


    fun setConversationData(){
        val messagePostFragment = supportFragmentManager.findFragmentById(R.id.fragment_container_post) as MessagePostFragment?
        messagePostFragment!!.getConversationData(conversationResponse!!.conversation!!.id, conversationResponse!!.conversation!!.sender_id,conversationResponse!!.conversation!!.receiver_id, conversationResponse!!.conversation!!.item_id)
    }


    override fun onCreateOptionsMenu(menu: Menu) : Boolean {
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                //NavUtils.navigateUpFromSameTask(this);
                Log.d(TAG, "Detail:")
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed(){
        super.onBackPressed()
    }
}