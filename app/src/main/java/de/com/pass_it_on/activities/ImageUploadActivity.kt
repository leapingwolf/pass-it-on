package de.com.pass_it_on.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import de.com.pass_it_on.R
import de.com.pass_it_on.fragments.ImageUploadFragment
import de.com.pass_it_on.models.ImageModel
import de.com.pass_it_on.utils.FragmentBackPressedListener

/**
 * Created by akshata on 16/09/16.
 */
class ImageUploadActivity : AppCompatActivity() {
    final val TAG : String = "ImageUploadActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "oncreate of test")
            // During initial setup, plug in the details fragment.
            setContentView(R.layout.activity_test_layout)
            val data = this.intent.extras
//            detailMenu = data.getInt("menu")
        if(savedInstanceState == null) {
            Log.d(TAG, "Image upload fragment new instance")
            val imageUploadFragment = ImageUploadFragment.newInstance(data.getParcelableArrayList<ImageModel>("images"),data.getParcelable("item"))
//            detailFragment.arguments = Bundle()
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, imageUploadFragment).commit()
        }
//            setResult(1, intent);
//            finish()
    }

    override fun onCreateOptionsMenu(menu: Menu) : Boolean {
        return true
    }

//    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
//        return super.onOptionsItemSelected(menuItem)
//    }



    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                //NavUtils.navigateUpFromSameTask(this);
                Log.d(TAG, "Detail:")
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed(){
        val fragments = supportFragmentManager.fragments
        val fragment = getLastNotNull(fragments)
        //nothing else in back stack || nothing in back stack is instance of our interface
        if (fragment !is FragmentBackPressedListener) {
            super.onBackPressed();
        } else {
            (fragment as FragmentBackPressedListener).doBack()
        }
    }

    private fun getLastNotNull(list: List<Fragment>): Fragment? {
        for (i in list.size - 1 downTo 0) {
            val frag = list[i]
            if (frag != null) {
                return frag
            }
        }
        return null
    }
}