package de.com.pass_it_on.activities;

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.webkit.*
import android.widget.FrameLayout
import android.widget.Toast
import com.google.gson.JsonParser
import de.com.pass_it_on.R
import de.com.pass_it_on.models.UserModel
import de.com.pass_it_on.network.HttpHandler
import de.com.pass_it_on.network.SessionManager
import de.com.pass_it_on.network.UserDataManager

class FbLoginActivity : AppCompatActivity() {
    final val TAG : String = "FbLoginActivity"
    private var mContext: Context? = null
    private var mWebview: WebView? = null
    private var mContainer: FrameLayout? = null
    private var session: SessionManager? = null
    private var pDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_fb_login)
        session = SessionManager(applicationContext)
        pDialog = ProgressDialog(this)
        pDialog!!.setCancelable(false)
        val cookieManager = CookieManager.getInstance()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.removeAllCookies { aBoolean -> // a callback which is executed when the cookies have been removed
                Log.d(TAG, "Cookie removed: " + aBoolean);
            }
        } else{cookieManager.removeAllCookie()}

        mWebview = findViewById(R.id.fbLoginWebView) as WebView?
        mWebview!!.clearCache(true)
        mWebview!!.clearFormData()
        mWebview!!.clearHistory()

        mContainer = findViewById(R.id.webview_frame) as FrameLayout?
        val webSettings = mWebview!!.settings
        Log.d(TAG, webSettings.cacheMode.toString())
        webSettings.javaScriptEnabled = true

        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.setSupportMultipleWindows(true)
        mWebview!!.setWebViewClient(UriWebViewClient())
        mWebview!!.setWebChromeClient(object : WebChromeClient() {
        override fun onProgressChanged(view: WebView, progress: Int){
            pDialog!!.setMessage("Loading.." + progress )
            pDialog!!.progress = progress
            if (progress == 100) {
                hideDialog()

            } else {
                showDialog()
            }
        }
        })
        mWebview!!.loadUrl(target_url)
        mWebview!!.addJavascriptInterface(MyJavaScriptInterface(this@FbLoginActivity), "HtmlViewer")
        mContext = this.applicationContext

    }


    override  fun onDestroy() {
        super.onDestroy()
        mWebview = null
    }
    private inner class UriWebViewClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url:String):Boolean{
            view.loadUrl(url)
//            hideDialog()
            return true
        }
        override fun onPageFinished(view: WebView?, url: String?) {
            if(url!!.startsWith(UserDataManager.FB_LOGIN_CALLBACK)) {
                mWebview!!.loadUrl("javascript:HtmlViewer.showHTML(document.body.getElementsByTagName('pre')[0].innerHTML);");
            }
        }

        override fun onReceivedSslError(view: WebView, handler: SslErrorHandler,
                                        error: SslError) {
            super.onReceivedSslError(view, handler, error);
            hideDialog()
        }
    }

    internal inner class MyJavaScriptInterface(private val ctx: Context) {

        @JavascriptInterface
        fun showHTML(html: String) {
            try {
                val parser = JsonParser()
                val json = parser.parse(html);
                Log.d(TAG, json.toString())
                val error =json.asJsonObject.get("error").toString()
                if (error.length > 2) {
                    val errors = HttpHandler.errors()
                    for (err: String in errors.keys) {
                        if (err == error) {
                            Log.d(TAG, "m  i here")
                            val toast = Toast.makeText(baseContext, errors.get(err).toString(), Toast.LENGTH_SHORT)
                            toast.show()
                        }
                    }
                } else {
                    Log.d(TAG, "m  i here 1")
                    val name = json.asJsonObject.get("user").asJsonObject.get("name").toString()
                    val email = json.asJsonObject.get("user").asJsonObject.get("email").toString()
                    val id = json.asJsonObject.get("user").asJsonObject.get("id").asInt
                    val anonymity =json.asJsonObject.get("user").asJsonObject.get("anonymity").asBoolean
                    val currentUser = UserModel(id, name, email, anonymity)
                    if(json.asJsonObject.get("authorization") !== null) {
                        val token = json.asJsonObject.get("authorization").asString
//                        val fcmToken = FirebaseInstanceId.getInstance().token
                        session!!.createLoginSession(currentUser.id, currentUser.name!!, currentUser.email!!, token)
                        Log.d(TAG, "values:" + currentUser!!.name + currentUser!!.email)
                        if(session!!.isLoggedIn) {
                            val intent = Intent(this@FbLoginActivity, HomeActivity::class.java)
                            startActivity(intent)
                            finish()
                        }
                    }
                }
            } catch(ex: Exception) {
                Log.d(TAG, ex.message.toString())
            }
        //    return true
                }
            }

    companion object {
        private val target_url = UserDataManager.FB_LOGIN
    }

    private fun showDialog() {
        if (!pDialog!!.isShowing)
            pDialog!!.show()
    }

    private fun hideDialog() {
        if (pDialog!!.isShowing)
            pDialog!!.dismiss()
    }
}
