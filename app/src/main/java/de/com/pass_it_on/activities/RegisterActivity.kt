package de.com.pass_it_on.activities


import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import de.com.pass_it_on.R
import de.com.pass_it_on.models.RegistrationResponse
import de.com.pass_it_on.models.RegistrationUser
import de.com.pass_it_on.network.HttpHandler
import de.com.pass_it_on.network.SessionManager
import de.com.pass_it_on.network.UserDataManager
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread


class RegisterActivity : AppCompatActivity() {
    final val TAG : String = "RegisterActivity"
    private  var btnRegister: Button? = null
    private var btnLinkToLogin: Button? = null
    private var inputFullName: EditText? = null
    private var inputEmail: EditText? = null
    private var inputPassword: EditText? = null
    private var inputPasswordConfirmation: EditText? = null
    private var pDialog: ProgressDialog? = null
    private var session: SessionManager? = null
    //    private SQLiteHandler db;

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        inputFullName = findViewById(R.id.name) as EditText?
        inputEmail = findViewById(R.id.email) as EditText?
        inputPassword = findViewById(R.id.password) as EditText?
        inputPasswordConfirmation = findViewById(R.id.passwordConfirmation) as EditText?
        btnRegister = findViewById(R.id.btnRegister) as Button?
        btnLinkToLogin = findViewById(R.id.btnLinkToLoginScreen) as Button?

        // Progress dialog
        pDialog = ProgressDialog(this)
        pDialog!!.setCancelable(false)

        // Session manager
        session = SessionManager(applicationContext)

        // SQLite database handler
        //        db = new SQLiteHandler(getApplicationContext());

        // Check if user is already logged in or not
        if (session!!.isLoggedIn) {
            // User is already logged in. Take him to main activity
            val intent = Intent(this@RegisterActivity,
                    HomeActivity::class.java)
            startActivity(intent)
            finish()
        }

        // Register Button Click event
        btnRegister!!.setOnClickListener(View.OnClickListener  {
            val name = inputFullName!!.text.toString().trim { it <= ' ' }
            val email = inputEmail!!.text.toString().trim { it <= ' ' }
            val password = inputPassword!!.text.toString().trim { it <= ' ' }
            val passwordConfirmation = inputPasswordConfirmation!!.text.toString().trim { it <= ' ' }

            if (!name.isEmpty() && !email.isEmpty() && !password.isEmpty()) {
                registerUser(name, email, password, passwordConfirmation)
            } else {
                Toast.makeText(applicationContext,
                        "Please enter your details!", Toast.LENGTH_LONG).show()
            }
        })

        // Link to Login Screen
        btnLinkToLogin!!.setOnClickListener(View.OnClickListener {
            val i = Intent(applicationContext,
                    LoginActivity::class.java)
            startActivity(i)
            finish()
        })

    }

    /**
     * Function to store user in MySQL database will post params(tag, name,
     * email, password) to register url
     */
    private fun registerUser(name: String, email: String,
                             password: String, passwordConfirmation: String) {
        // Tag used to cancel the request
        try {
            //val registrationUser = RegistrationUser("akshata", "12345", "12345", "identity", "", "akshatahan@gmail.com")
            val registrationUser = RegistrationUser(name, password, passwordConfirmation, "identity", email)
            pDialog!!.setMessage("Registering ...")
            val inputManager = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(inputPassword!!.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            showDialog()

            async() {
//                val result = HttpHandler.post(this@RegisterActivity, HttpHandler.IDENTITY_CALLBACK, registrationUser)
                val result = UserDataManager.post(UserDataManager.IDENTITY_CALLBACK, registrationUser)
                uiThread {
                    result!!.success {
                        val registrationResponse = result!!.component1() as RegistrationResponse
                        if (registrationResponse.error.length > 0) {
                            Log.d(TAG, "registration error" + registrationResponse.error)
                            val errors = HttpHandler.errors()
                            for (err: String in errors.keys) {
                                if (err == registrationResponse.error) {
                                    hideDialog()
                                    val toast = Toast.makeText(baseContext, errors.get(err).toString(), Toast.LENGTH_LONG)
                                    toast.show()
                                }
                            }
                        } else {
                            hideDialog()
                            val toast = Toast.makeText(baseContext, R.string.check_email, Toast.LENGTH_SHORT)
                            toast.show()
                        }
                    }
                    result!!.failure {
                        Log.d(TAG, "error")
                    }
                }
            }
        }
        catch(ex: Exception){
            Log.d(TAG,ex.message.toString())
        }
    }

    private fun showDialog() {
        if (!pDialog!!.isShowing)
            pDialog!!.show()
    }

    private fun hideDialog() {
        if (pDialog!!.isShowing)
            pDialog!!.dismiss()
    }

    companion object {
        private val TAG = RegisterActivity::class.java.simpleName
    }
}