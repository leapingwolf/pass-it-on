package de.com.pass_it_on.activities

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log

class SearchActivity : AppCompatActivity() {
    final val TAG : String = "SearchActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.search);
        Log.d(TAG, "in search")
        // Get the intent, verify the action and get the query
        val intent = Intent()
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            val query = intent.getStringExtra(SearchManager.QUERY);
            doMySearch(query);
        }
    }

    private fun doMySearch(query: String) {
        Log.d(TAG,query);
    }
}
