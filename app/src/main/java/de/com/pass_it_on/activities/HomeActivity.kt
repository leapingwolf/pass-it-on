package de.com.pass_it_on.activities

//import kotlinx.android.synthetic.main.activity_home.*
import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.iid.FirebaseInstanceId
import de.com.pass_it_on.App
import de.com.pass_it_on.R
import de.com.pass_it_on.fragments.AllItemListFragment
import de.com.pass_it_on.fragments.ConversationFragment
import de.com.pass_it_on.fragments.MyItemListFragment
import de.com.pass_it_on.models.*
import de.com.pass_it_on.network.*
import de.com.pass_it_on.utils.AlertDialogFragment
import de.com.pass_it_on.utils.GoogleApiHelper
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import pl.tajchert.nammu.Nammu
import pl.tajchert.nammu.PermissionCallback

class HomeActivity : AppCompatActivity(),
//        MessageFragment.OnFragmentInteractionListener,
        MyItemListFragment.OnListFragmentInteractionListener,
        AllItemListFragment.OnListFragmentInteractionListener,
        ConversationFragment.OnListFragmentInteractionListener,
        HttpInterface {

    final val TAG : String = "HomeActivity"
    lateinit  var mViewPager: ViewPager
    lateinit var mSectionsPagerAdapter: SectionsPagerAdapter;
    private var headerProgress: ProgressBar? = null
    lateinit  var mFragmentTags: MutableMap<Int,String>
    private var session: SessionManager? = null
    lateinit var  currentUser: UserModel

    private var currentLongitude: Float? =null
    private var currentLatitude: Float? =null
    var mGoogleApiClient: GoogleApiClient? = null
    lateinit var mGoogleApiHelper: GoogleApiHelper

    private fun checkLocationPermission(){
        Log.d(TAG,"permission check")
        val permissionCheck = Nammu.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION)
        if(permissionCheck == true){
//            Log.d(TAG,"Loction availability" + FusedLocationApi.getLocationAvailability(mGoogleApiClient).isLocationAvailable)
//            val mGoogleHelperApiClient = mGoogleApiHelper.googleApiClient
//            val location = LocationServices.FusedLocationApi.getLastLocation(mGoogleHelperApiClient);
//            val location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if(mGoogleApiHelper.isConnected){
               val location = mGoogleApiHelper.currentLocation
                if(location != null) {
                    currentLatitude = location!!.latitude.toFloat()
                    currentLongitude = location!!.longitude.toFloat()
                }
//                Log.d(TAG,"location when permission is present:" + location.latitude)
            }
            Log.d(TAG,"googleapihelper connected: " + mGoogleApiHelper.isConnected)
        }
        else {
//            if (Nammu.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
//                Log.d(TAG, "show permission rationale")
//                //User already refused to give us this permission or removed it
//                //Now he/she can mark "never ask again" (sic!)
//                Toast.makeText(this," need to design alert dialog", Toast.LENGTH_SHORT).show()
////                val adb = AlertDialog.Builder(this)
////                adb.setMessage("Need access to locations for bla bla bla").setCancelable(true)
////                        .setPositiveButton("OK", DialogInterface.OnClickListener{ dialogInterface, i ->
////                            Nammu.askForPermission(this, Manifest.permission.ACCESS_FINE_LOCATION, permissionLocationCallback)
////                        })
////                        .setNegativeButton("Cancel",  DialogInterface.OnClickListener { dialogInterface, i ->
////                            currentLatitude = null
////                            currentLongitude = null
////                        })
////                adb.show()
//            } else {
//                First time asking for permission
//                 or phone doesn't offer permission
//                 or user marked "never ask again"
                Log.d(TAG, "if user had clicked never ask permission or first time or settings location is turned off")
                Nammu.askForPermission(this, Manifest.permission.ACCESS_FINE_LOCATION,permissionLocationCallback)
         //   }
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray){
//        Toast.makeText(this, "request is " + requestCode,Toast.LENGTH_SHORT).show()
        Nammu.onRequestPermissionsResult(requestCode, permissions,grantResults)
    }

    override fun onDestroy(){
        super.onDestroy()
        mGoogleApiHelper.disconnect()
//        mGoogleApiClient!!.disconnect()
    }

    internal val permissionLocationCallback: PermissionCallback = object : PermissionCallback {
        override fun permissionGranted() {
            //Toast.makeText(this@HomeActivity,"permission granted", Toast.LENGTH_SHORT).show()
            if(mGoogleApiHelper.isConnected){
                val location = mGoogleApiHelper.currentLocation
                if(location != null) {
                    currentLatitude = location!!.latitude.toFloat()
                    currentLongitude = location!!.longitude.toFloat()
                    if(mViewPager.currentItem == 1) {
                        var currentFragment = mSectionsPagerAdapter.getFragment(1) as AllItemListFragment?
                        if (currentFragment != null) {
                            //refresh the view with distances
                            currentFragment.update(ItemDataManager.ALL_ITEMS, currentLatitude, currentLongitude)
                        }
                    }
                    Log.d(TAG,"location when permission is granted:" + location.latitude)
 //           val location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiHelperClient);
//            val location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//            if (location == null) {
//                Toast.makeText(this@HomeActivity, "Location settings is off", Toast.LENGTH_LONG).show()
//            } else {
//                //If everything went fine lets get latitude and longitude
//                currentLatitude = location.latitude.toFloat()
//                currentLongitude = location.longitude.toFloat()

                //only update if the current view is that of All Items

            }
            Log.d(TAG,"google connection helper status in granted:" + mGoogleApiHelper.isConnected)
               // Toast.makeText(this@HomeActivity, currentLatitude.toString() + " WORKS " + currentLongitude + "", Toast.LENGTH_LONG).show();
            }
            //            boolean hasAccess = true
            //            Toast.makeText(MainActivity.this, "Access granted = " + hasAccess, Toast.LENGTH_SHORT).show();
        }
        override fun permissionRefused() {
            //Toast.makeText(this@HomeActivity,"in nammu callback for refused", Toast.LENGTH_SHORT).show()
            currentLatitude = null
            currentLongitude = null
            //            boolean hasAccess = Tools.accessContacts(MainActivity.this);
            //            Toast.makeText(MainActivity.this, "Access granted = " + hasAccess, Toast.LENGTH_SHORT).show();
        }
    }

    override fun getMyItems(pageNumber: Int, url: String,query: String?, orderBy: Int): Result<Array<ItemModel>, FuelError>?  {
        if (! session!!.isLoggedIn) {
            val toast = Toast.makeText(this@HomeActivity, R.string.invalid_user_session, Toast.LENGTH_SHORT)
            toast.show()
            val intent = Intent(this@HomeActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        val token = session!!.userDetails[SessionManager.KEY_TOKEN]

        Log.d(TAG, "calling get url:")
        val result = HttpHandler.getMyItems(url, pageNumber, token!!, query, orderBy)
        //val arrItemUserModel = result!!.component1()!! as Array<ItemUserModel>?
        return result
    }

    override fun getAllItems(url: String, pageNumber: Int, query: String?, latitude: String?, longitude: String?, orderBy: Int): Result<Array<ItemUserModel>, FuelError>? {
        if (! session!!.isLoggedIn) {
            val toast = Toast.makeText(this@HomeActivity, R.string.invalid_user_session, Toast.LENGTH_SHORT)
            toast.show()
            val intent = Intent(this@HomeActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        val token = session!!.userDetails[SessionManager.KEY_TOKEN]
        val result = HttpHandler.getItems(url, pageNumber, token!!, query, latitude, longitude,orderBy)
        return result
    }

    override fun getConversations(url: String, pageNumber: Int, orderBy: Int): Result<Array<ConversationResponse>, FuelError>? {
        if (! session!!.isLoggedIn) {
            val toast = Toast.makeText(this@HomeActivity, R.string.invalid_user_session, Toast.LENGTH_SHORT)
            toast.show()
            val intent = Intent(this@HomeActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        val token = session!!.userDetails[SessionManager.KEY_TOKEN]
        val result = HttpHandler.getConversations(url, pageNumber, token!!,orderBy)
        return result
    }

//    override fun getItems(pageNumber: Int, url: String,query: String?, orderBy: Int):  Result<Any, FuelError>?  {
//        //val arrItemUserModel = HttpHandler.get(HttpHandler.API_URL + url + "?" + "page=" + pageNumber , HttpHandler.hardCodedAuthToken)
//        if (! session!!.isLoggedIn) {
//            val toast = Toast.makeText(this@HomeActivity, R.string.invalid_user_session, Toast.LENGTH_SHORT)
//            toast.show()
//            val intent = Intent(this@HomeActivity, LoginAct::class.java)
//            startActivity(intent)
//            finish()
//        }
//
//            val token = session!!.userDetails[SessionManager.KEY_TOKEN]
//
//            Log.d("testing", "calling get url:")
//            val result = HttpHandler.getItems(url, pageNumber, token!!, query, orderBy)
//                //val arrItemUserModel = result!!.component1()!! as Array<ItemUserModel>?
//            return result
//    }

//    override fun getItems(query: String?, url: String): Array<ItemUserModel>? {
//        //TODO: hardcoded pageNumber to 1
//        Log.d(TAG,"or here atleast")
//        val arrItemUserModel = HttpHandler.get(HttpHandler.API_URL + url , 1 , HttpHandler.hardCodedAuthToken, query)
//        return arrItemUserModel
//    }

    /**
     * The [android.support.v4.view.PagerAdapter] that will provide
     * fragments for each of the sections. We use a
     * [FragmentPagerAdapter] derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * [android.support.v4.app.FragmentStatePagerAdapter].
     */
    //private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    /**
     * The [ViewPager] that will host the section contents.
     */
    //private var mViewPager: ViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
//        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        Nammu.init(this)
        mGoogleApiHelper = App.googleApiHelper
        mGoogleApiHelper.connect()
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .build();



        currentUser = getCurrentSession()!!

        if (currentUser == null){
            val toast = Toast.makeText(this@HomeActivity, R.string.invalid_user_session, Toast.LENGTH_SHORT)
            toast.show()
            val intent = Intent(this@HomeActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }

        val fcmToken = FirebaseInstanceId.getInstance().token
        if(fcmToken != null){
            Log.d(TAG,"get instance in homeactivity" + fcmToken)
            if(session!!.getFCMToken() != fcmToken) {
                session!!.saveFCMToken(fcmToken!!)
            }
                //send FCM token to server to save in database
                val fcmTokenModel = FcmtokenModel(null, fcmToken, currentUser.id)
                val token = session!!.userDetails[SessionManager.KEY_TOKEN]
                async() {
                    val result = FcmtokenDataManager.post(FcmtokenDataManager.FCMTOKENS, fcmTokenModel, token!!)
                    result!!.success { Log.d(TAG, "logged out successfully") }
                    result!!.failure { Log.d(TAG, "error is" + result!!.component2().toString()) }
                }
            }

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        mFragmentTags= hashMapOf<Int,String>()
        val appBarLayout = findViewById(R.id.appbar) as AppBarLayout


//        headerProgress!!.visibility = View.GONE
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container) as ViewPager
        headerProgress = mViewPager!!.findViewById(R.id.pbHeaderProgress) as ProgressBar?
        mViewPager!!.adapter = mSectionsPagerAdapter

        val tabLayout = findViewById(R.id.tabs) as TabLayout
        tabLayout.setupWithViewPager(mViewPager)


        val fab = findViewById(R.id.fab) as FloatingActionButton?
        fab!!.setOnClickListener {
            //view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show() }
//            Toast.makeText(this@HomeActivity, "" + currentLatitude + "&&" + currentLongitude, Toast.LENGTH_SHORT).show()
            val intent = Intent(this@HomeActivity, ItemDetailActivity::class.java)
            intent.putExtra("editable", true)
            intent.putExtra("addition", true)
            intent.putExtra("menu", R.menu.menu_myitems_additem)
            startActivityForResult(intent,1)

            }
//        floatingActionButton.visibility = View.VISIBLE

        //val fab = findViewById(R.id.fab) as FloatingActionButton
        mViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                Log.d(TAG, "on Page selected: " + position)
                appBarLayout.setExpanded(true,true)
                when(position){
                    0  -> {
                    var currentFragment = mSectionsPagerAdapter.getFragment(position) as MyItemListFragment?
                        if(currentFragment != null){
                            currentFragment.update(UserDataManager.ALL_USERS + "/" + currentUser.id + "/items")
                        }
                    }
                    1 -> {
                        var currentFragment = mSectionsPagerAdapter.getFragment(position) as AllItemListFragment?
                        if(currentFragment != null){
                            if(mGoogleApiHelper.isConnected) {
                               // Toast.makeText(this@HomeActivity,"api helper is connected", Toast.LENGTH_SHORT).show()
                                checkLocationPermission()
                            }
                            currentFragment.update( ItemDataManager.ALL_ITEMS,currentLatitude,currentLongitude)
                        }
                    }
                    2 -> {
                        var currentFragment = mSectionsPagerAdapter.getFragment(position) as ConversationFragment?
                        if(currentFragment != null){
                            currentFragment.update(ConversationDataManager.CONVERSATIONS)
                        }
                    }
                }
                if (position == 0) {
                    fab.visibility = View.VISIBLE
                } else {
                    fab.visibility = View.GONE
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }

    private fun getCurrentSession(): UserModel? {
        session = SessionManager(applicationContext)
        return session!!.checkLogin()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.menu_home_itemlist, menu)
        return true
    }

    override  fun onBackPressed(){
        super.onBackPressed()
        Log.d(TAG, "back pressed")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        if (id == R.id.home) {
            onBackPressed()
        }
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true
        }

        if (id == R.id.action_exit) {
            if (session!!.isLoggedIn) {
                val dlgAlert = android.app.AlertDialog.Builder(this)
                dlgAlert.setMessage(resources.getString(R.string.logout))
                dlgAlert.setTitle(resources.getText(R.string.app_name))
                dlgAlert.setPositiveButton("OK", DialogInterface.OnClickListener { dialogInterface, i ->
                    dialogInterface.dismiss()
                    logout()
                })
                dlgAlert.setNegativeButton("CANCEL", DialogInterface.OnClickListener { dialogInterface, i ->
                    //do nothing
                })
                dlgAlert.setCancelable(true)
                dlgAlert.create()
                dlgAlert.show()
            }
        }
        return super.onOptionsItemSelected(item)

    }


    fun logout(){
        async() {
            val result = FcmtokenDataManager.delete(FcmtokenDataManager.FCMTOKENS, session!!.userDetails[SessionManager.KEY_TOKEN], FirebaseInstanceId.getInstance().token,currentUser.id)
            result!!.success {
                Log.d(TAG, "fcm token  deleted successfully")
                val result = UserDataManager.delete( UserDataManager.LOGOUT, session!!.userDetails[SessionManager.KEY_TOKEN])
                result!!.success { Log.d(TAG, "logged out successfully")}
                result!!.failure { Log.d(TAG, "log out failed") }
            }
            result!!.failure { Log.d(TAG, "token delete out failed") }
            uiThread {
                Log.d(TAG, "logged out and token deleted")
                session!!.logoutUser()
                val intent = Intent(this@HomeActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    override fun onListFragmentInteraction(item: ConversationResponse) {
        val intent = Intent(this@HomeActivity, ConversationDetailActivity::class.java)
        intent.putExtra("conversationResponse", item)
        startActivity(intent)
    }

    override fun onListFragmentInteraction(item: ItemModel) {
        val intent = Intent(this@HomeActivity, ItemDetailActivity::class.java)
        intent.putExtra("item", item)
        intent.putExtra("editable", true)
        intent.putExtra("addition", false)
        intent.putExtra("menu", R.menu.menu_myitems_edititem)
        startActivityForResult(intent,0)
    }

    override fun onListFragmentInteraction(itemUser: ItemUserModel) {
        //do nothing
                val intent = Intent(this@HomeActivity, ItemDetailActivity::class.java)
                intent.putExtra("item", itemUser!!.item)
                intent.putExtra("user", itemUser!!.user)
                intent.putExtra("editable", false)
                intent.putExtra("addition", false)
                intent.putExtra("menu", R.menu.menu_home_viewitem)
                startActivityForResult(intent,1)
        }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d(TAG,"back to activity")
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            HttpHandler.UPDATE -> {
                Log.d(TAG, "updating after back from my items")
                val fragment = mSectionsPagerAdapter.getFragment(0) as MyItemListFragment
                fragment.update(UserDataManager.ALL_USERS + "/" + currentUser.id + "/items")
            }
            HttpHandler.NO_UPDATE -> {
                Log.d(TAG, "not updating after return from view item")
            }
        }
    }

//    override fun onResume(){
//        super.onResume()
//        //var myItemsFragment = mSectionsPagerAdapter.getItem(0)
//        Log.d(TAG, "activity is resumed")
////        fragmentManager.
//    }

    /**
     * A placeholder fragment containing a simple view.
     */


    /**
     * A [FragmentPagerAdapter] that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            Log.d(TAG, "instantiate item")
            var obj = super.instantiateItem(container, position)

            if (obj is Fragment) {
                Log.d(TAG, "here")

//                if (mFragmentTags != null) {
                mFragmentTags.put(position,obj.tag)
                Log.d(TAG, "not here")
            }
                    return obj
//                }
            }

        fun getFragment(position: Int): Fragment?{
            val tag = mFragmentTags!!.get(position)
            if (tag == null)
                return null
            return supportFragmentManager.findFragmentByTag(tag)
        }

        override fun getItem(position: Int): Fragment? {
            Log.d(TAG, "create fragment")

            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            when (position) {
//                0 -> return ProfileFragment.newInstance(position)
                0 ->{
                    return MyItemListFragment.newInstance(position, UserDataManager.ALL_USERS + "/" + currentUser.id  + "/items" , R.menu.menu_home_itemlist )
                    //return MyItemListFragment.newInstance(position, HttpHandler.ALL_USERS + currentUser.id + "/items",R.menu.menu_home_itemlist )
                }
                1 ->// return MessageFragment.newInstance("","")
                    return AllItemListFragment.newInstance(0,  ItemDataManager.ALL_ITEMS, R.menu.menu_home_itemlist)
                2 -> {
                    return ConversationFragment.newInstance(0, ConversationDataManager.CONVERSATIONS)
//                    return MessageFragment.newInstance("","")
                }
//                2 -> return TestFragmentFragment.newInstance(0)
            }
            return null

        }

        override fun getCount(): Int {
            // Show 3 total pages.
            return 3
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when (position) {
                0 -> return "My Listings"
                1 -> return "Offers"
                2 -> return "Messages"
            }
            return null
        }
    }
}
