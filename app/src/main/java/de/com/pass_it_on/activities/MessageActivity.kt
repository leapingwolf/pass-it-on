package de.com.pass_it_on.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import de.com.pass_it_on.R
import de.com.pass_it_on.fragments.MessageFragment
import de.com.pass_it_on.fragments.MessagePostFragment
import de.com.pass_it_on.models.ItemModel
import de.com.pass_it_on.models.MessageResponse
import de.com.pass_it_on.models.UserModel
import de.com.pass_it_on.network.ConversationDataManager
import de.com.pass_it_on.network.SessionManager
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import java.util.*


class MessageActivity: AppCompatActivity(), MessagePostFragment.MessagePassingInterface{
    final val TAG : String = "MessageActivity"
    override fun passMessagePostResultSet(messageResponseArray: ArrayList<MessageResponse?>) {
        Log.d(TAG, "needs to be implemented")
        val messageFragment = supportFragmentManager.findFragmentById(R.id.fragment_container) as MessageFragment?
        messageFragment!!.update(messageResponseArray)
    }
    //    override fun getConversationData(conversationId: Int?, enquirerId: Int?, itemOwnerId: Int?) {
////        Toast.makeText(this, conversationId.toString() ,Toast.LENGTH_SHORT).show()
//        val messagePostFragment = supportFragmentManager.findFragmentById(R.id.fragment_container1) as MessagePostFragment?
//        messagePostFragment!!.setConversationData(conversationId, enquirerId,itemOwnerId)
//    }

    private lateinit var subject: TextView
    private lateinit var to: TextView
    private lateinit var messageText: EditText
    private lateinit var itemTitle: TextView
    private lateinit var itemImageThumbnail: ImageView
    lateinit var token: String
    private  var currentUser: UserModel? = null
    private var headerProgress: ProgressBar? = null
    lateinit var mValues: ArrayList<MessageResponse?>
    var mConversationId: Int? =null
    var mEnquirerId: Int? = null
    var mItemOwnerId: Int? = null
    var item: ItemModel? = null
    var user: UserModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)
        if (savedInstanceState == null) {

            val picasso = Picasso.Builder(this)
                    .listener { picasso, uri, exception -> Log.d(TAG, exception.toString()) }
                    .build()
//        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        if (savedInstanceState == null) {
            val extras = intent.extras
            item = extras.getParcelable<ItemModel?>("item")
            user = extras.getParcelable<UserModel?>("user")
            if(item == null && user == null){
                // notification is clicked
                var gson = GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm").create()
                user = gson.fromJson(extras.get("user").toString() , UserModel::class.java)
                item = gson.fromJson(extras.get("item").toString() , ItemModel::class.java)
            }

//            if(item == null and user == null){
//                //
//            }
            val session = SessionManager(applicationContext)
            currentUser = session!!.checkLogin()

            if (currentUser == null) {
                val toast = Toast.makeText(this@MessageActivity, R.string.invalid_user_session, Toast.LENGTH_SHORT)
                toast.show()
                val intent = Intent(this@MessageActivity, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
            token = session!!.userDetails[SessionManager.KEY_TOKEN].toString()

            //start conversation with itemowner
            title = "Conversation with " + user!!.name

            headerProgress = findViewById(R.id.pbHeaderProgress) as ProgressBar?
            headerProgress!!.visibility = View.GONE
//            setSupportActionBar(toolbar)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)

//            subject = findViewById(R.id.subject) as TextView
//            subject!!.text = itemUser!!.item?.title ?: ""
//
//            to = findViewById(R.id.to) as TextView
//            to!!.text = itemUser!!.user?.name ?: ""

//            messageText = findViewById(R.id.message) as EditText

            itemTitle = findViewById(R.id.title) as TextView
            itemTitle!!.text = item!!.title

            itemImageThumbnail = findViewById(R.id.thumbnail) as ImageView
            if (item!!.image!! != null && item!!.image!!.count() > 0) {
                picasso.load(item!!.image!![0]!!.url + ".png").placeholder(R.drawable.thumbnail_launcher).into(itemImageThumbnail)
            }

            val messageFragment = MessageFragment.newInstance()
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, messageFragment).commit()
            asyncTask()

            val messagePostFragment = MessagePostFragment.newInstance()
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container_post, messagePostFragment).commit()


//            val btnSend = findViewById(R.id.btnSend) as ImageButton?
//            btnSend!!.setOnClickListener {
//                if (messageText!!.text.length > 0) {
//                    Log.d(TAG, "enter message for item:" + itemUser!!.item!!.id.toString())
//                    Log.d(TAG, "enter message for sender:" + itemUser!!.user!!.id.toString())
//                    val message = MessageModel(currentUser!!.id, null, itemUser!!.user!!.id, null,messageText!!.text.toString(), null)
//                    val arrMessageModel = ArrayList<MessageModel?>()
//                    arrMessageModel.add(message)
//                    val conversation = ConversationModel(null, itemUser!!.item!!.id, null, arrMessageModel)
//                    headerProgress!!.visibility = View.VISIBLE
//                    this.window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                    async() {
//                        val result = HttpHandler.put(HttpHandler.CONVERSATIONS, conversation, token)
//                        result!!.success {
//                            Log.d(TAG, "conversation inserted")
//                            runOnUiThread {
//                                messageFragment.update(result!!.component1())
//                                messageText.text = null
//                                headerProgress!!.visibility = View.GONE
//                                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
//                            }
//                        }
//                        result!!.failure {
//
//                            runOnUiThread {
//                                Log.d(TAG, "in post error" + result!!.component2()!!.message.toString())
//                                headerProgress!!.visibility = View.GONE
//                                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
//                            }
//                        }
//                        uiThread {
//                            Log.d(TAG, "done")
////                            setResult(100)
////                            finish()
//                        }
//                    }
//                } else {
//                    messageText.error = "Message is required"
//                }
//            }
        }
    }

    fun asyncTask() {
        headerProgress!!.visibility = 1
        mValues = ArrayList<MessageResponse?>()
        async() {
            var result: Result<Array<MessageResponse>, FuelError>? = null
            if(item != null) {
                //result  = HttpHandler.getMessages(HttpHandler.API_URL + "/auth/conversations",  token, currentUser!!.id, user!!.id, item!!.id)
                result  = ConversationDataManager.get(ConversationDataManager.CONVERSATIONS,  token, currentUser!!.id, user!!.id, item!!.id)
            }
            result!!.success {
                Log.d(TAG, "query result returned")
                val messageArray = result!!.component1()
                runOnUiThread() {
                    if (messageArray != null) {
                        Log.d(TAG, "conversation is not null")
                        mValues!!.clear()
                        if (messageArray!!.count() > 0) {
                            //send the itemOwner and enquired id - taken from the conversation record = first recorded message
                            setConversationData(messageArray[0]!!.message!!.conversation_id!!, messageArray[0]!!.message!!.sender_id!!, messageArray[0]!!.message!!.receiver_id!!)
                            mValues!!.addAll(messageArray.toList())
                        } else {
                            //first conversation
                            setConversationData(null, currentUser!!.id, user!!.id)
                        }
                    }
                }
            }

            result!!.failure {
                runOnUiThread {
                    Log.d(TAG, result!!.component2()!!.toString())
                    Log.d(TAG, "conversation not fetched")
                    headerProgress!!.visibility = View.GONE
                    val toast = Toast.makeText(this@MessageActivity, R.string.ConnectionError, Toast.LENGTH_SHORT)
                    toast.show()
                }
            }
            uiThread {
                headerProgress!!.visibility = View.GONE
                val messageFragment = supportFragmentManager.findFragmentById(R.id.fragment_container) as MessageFragment?
                messageFragment!!.update(mValues)
            }
        }
    }

    fun setConversationData(conversationId: Int?, enquirerId: Int?, itemOwnerId: Int?){
        mConversationId = conversationId
        mEnquirerId = enquirerId
        mItemOwnerId = itemOwnerId
        val messagePostFragment = supportFragmentManager.findFragmentById(R.id.fragment_container_post) as MessagePostFragment?
        messagePostFragment!!.getConversationData(conversationId, enquirerId,itemOwnerId, item!!.id)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                //NavUtils.navigateUpFromSameTask(this);
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

}


