package de.com.pass_it_on.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.WindowManager
import de.com.pass_it_on.R
import de.com.pass_it_on.fragments.ItemDetailFragment
import de.com.pass_it_on.models.ItemModel
import de.com.pass_it_on.models.UserModel
import de.com.pass_it_on.utils.FragmentBackPressedListener


class ItemDetailActivity : AppCompatActivity(), ItemDetailFragment.OnFragmentInteractionListener{
//        ActivityCompat.OnRequestPermissionsResultCallback {
//    private val callingActivity: Int = R.string.title_activity_item_detail
    final val TAG : String = "ItemDetailActivity"
    var detailMenu: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(TAG, "oncreate item detail")
        super.onCreate(savedInstanceState)
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (savedInstanceState == null) {
            // During initial setup, plug in the details fragment.
            setContentView(R.layout.activity_item_detail)
            val data = this.intent.extras
//            detailMenu = data.getInt("menu")
            val detailFragment = ItemDetailFragment.newInstance(0,data.getParcelable<ItemModel>("item"), data.getParcelable<UserModel?>("user"),data.getInt("menu"),data.getBoolean("editable"), data.getBoolean("addition"))
//            detailFragment.arguments = Bundle()
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, detailFragment).commit()
//            setResult(1, intent);
//            finish()
        }
    }


//    override fun onRequestPermissionsResult(requestCode: Int,
//                                            permissions: Array<String>,
//                                            grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//    }
    override fun onCreateOptionsMenu(menu: Menu) : Boolean {
//        Log.d(TAG, callingActivity.className)
//        if(callingActivity.className == "HomeActivity"){
//            menuInflater.inflate(R.menu.menu_home_viewitem, menu);
//        }
//        else if(callingActivity.className == "MyItemsActivity"){
            //menuInflater.inflate(detailMenu, menu);
  //      }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

//        val fragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
//        fragment.onActivityResult(requestCode, resultCode, data);
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                //NavUtils.navigateUpFromSameTask(this);
                Log.d(TAG, "Detail:")
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed(){
        val fragments = supportFragmentManager.fragments
        val fragment = getLastNotNull(fragments)
        //nothing else in back stack || nothing in back stack is instance of our interface
        if (fragment !is FragmentBackPressedListener) {
            super.onBackPressed();
        } else {
            (fragment as FragmentBackPressedListener).doBack()
        }
    }

    private fun getLastNotNull(list: List<Fragment>): Fragment? {
        for (i in list.size - 1 downTo 0) {
            val frag = list[i]
            if (frag != null) {
                return frag
            }
        }
        return null
    }

    override fun onFragmentInteraction(uri: Uri) {
        //do something here
    }

}
