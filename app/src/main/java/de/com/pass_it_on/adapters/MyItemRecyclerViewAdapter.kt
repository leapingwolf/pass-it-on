package de.com.pass_it_on.adapters;

//import com.aerialroots.aerialroots.fragmenttest.dummy.DummyContent.DummyItem
//import com.aerialroots.aerialroots.fragmenttest.dummy.DummyContent.DummyItem
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import de.com.pass_it_on.R
import de.com.pass_it_on.fragments.MyItemListFragment
import de.com.pass_it_on.models.ItemModel
import java.text.SimpleDateFormat

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class MyItemRecyclerViewAdapter(private val mValues: List<ItemModel>,
                                private val mListener: MyItemListFragment.OnListFragmentInteractionListener?,
                                private val picasso: Picasso)
        : RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder>(){
    final val TAG : String = "MyItemRecyclerViewAdapter"
    lateinit var context: Context
//    lateinit var picasso: Picasso
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_myitem, parent, false)
        context = parent.context
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = mValues[position]
        holder.mContentView.text = mValues[position]!!.title
        //val imageUrl = "https:/6/unsplash.com/photos/x7HJdJZqplo"
//        var imageUrl: String =
//        var imageUrl = "http://contentservice.mc.reyrey.net/image_v1.0.0/?id=2774412"
        if(mValues[position]!!.image!!.count()>0) {
            picasso.load(mValues[position]!!.image!![0]!!.url + ".png").placeholder(R.drawable.thumbnail_launcher).fit().into(holder.mImageView)
        }else{
            picasso.load(R.drawable.thumbnail_launcher).placeholder(R.drawable.thumbnail_launcher).fit().into(holder.mImageView)
        }
//        holder.mItemUpdatedDate.text = "edited: " + formatDate(mValues[position]!!.updated_at.toString())
        holder.mItemInsertedDate.text =  formatDate(mValues[position]!!.inserted_at.toString())
        if (mValues[position].offerType != null){
            if (mValues[position].offerType == 2) { holder.mOfferType.text = "NEED"  } else {
                holder.mOfferType.text = "GIVE"  }}
//        holder.mItemUpdatedDate.text = formatDate(mValues[position]!!.inserted_at.toString())

//        if(mOrderByQuery == 1) {//show updated date
//            holder.mItemUpdatedText.text = "Updated on"
//            holder.mItemUpdatedDate.text =formatDate(mValues[position].updated_at.toString())
//        } //show inserted date
//        else{
//            holder.mItemUpdatedText.text = "Added on"
//            holder.mItemUpdatedDate.text =formatDate(mValues[position].inserted_at.toString())
//        }

        holder.mView.setOnClickListener {
            mListener?.onListFragmentInteraction(holder.mItem)
        }
    }



    override fun getItemCount(): Int {
        return mValues.size
    }

    private fun formatDate(postedDate: String?): String? {
        if (postedDate != null) {
//            Log.d(TAG, " raw updated date" + postedDate)

            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val formattedDate = SimpleDateFormat("dd.MM.yyyy hh:mm")
            try {
                val d = dateFormat.parse(postedDate)
                System.out.println("DATE" + d);
                return formattedDate.format(d)
            } catch(e: Exception) {
                System.out.println("Excep" + e.message);
            }
        }
        return null
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mContentView: TextView
        val mImageView: ImageView
//        val mItemUpdatedDate: TextView
        val mItemInsertedDate: TextView
        val mOfferType: TextView
        lateinit var mItem: ItemModel

        init {
            mContentView = mView.findViewById(R.id.title) as TextView
            mImageView = mView.findViewById(R.id.image) as ImageView
//            mItemUpdatedDate = mView.findViewById(R.id.itemUpdatedDate) as TextView
            mItemInsertedDate = mView.findViewById(R.id.itemInsertedDate) as TextView
            mOfferType = mView.findViewById(R.id.offerType) as TextView
        }

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
