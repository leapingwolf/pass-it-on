package de.com.pass_it_on.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ProgressBar
import com.squareup.picasso.Picasso
import de.com.pass_it_on.R
import de.com.pass_it_on.models.ImageModel
import de.com.pass_it_on.models.ImageUploadModel
import org.jetbrains.anko.onClick
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by akshata on 16/09/16.
 */
class ImageUploadViewAdapter(private val context: Context, private val mValues: List<ImageUploadModel>, val onClickListener: ImageUploadAdapterListener)
: RecyclerView.Adapter<ImageUploadViewAdapter.ViewHolder>(){
    final val TAG : String = "ImageUploadViewAdapter"
//    lateinit var context: Context
    val mContext = context

    interface ImageUploadAdapterListener{
        fun cameraBtnOnClick(v: View ,position: Int)
        fun retryBtnOnClick(v: View ,position: Int)
        fun galleryBtnOnClick(v: View ,position: Int)
    }
    //    lateinit var picasso: Picasso
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        Log.d(TAG, "on create view holder")
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_image_upload, parent, false)
//        context = parent.context
        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.mImage = mValues[position]
        Log.d(TAG, "on bind view holder")
        holder.bind(mValues[position], onClickListener)
    }

    fun uploadImages(images: ArrayList<ImageModel>){
        Log.d(TAG,"m i here")
    }

    private fun formatDate(postedDate: String?): String? {
        if (postedDate != null) {
//            Log.d(TAG, " raw updated date" + postedDate)
            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val formattedDate = SimpleDateFormat("dd.MM.yyyy hh:mm");
            try {
                val d = dateFormat.parse(postedDate)
                System.out.println("DATE" + d);
                return formattedDate.format(d)
            } catch(e: Exception) {
                System.out.println("Excep" + e.message);
            }
        }
        return null
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mBtnCamera: ImageButton
        val mBtnGallery: ImageButton
        val mImageView: ImageView
        var mProgressBar: ProgressBar
        var mRetryButton: ImageButton
        lateinit var mImage: ImageModel
        lateinit var picasso: Picasso


        init {
            Log.d(TAG, "init viewholder")
            mBtnCamera = mView.findViewById(R.id.btnImageCapture) as ImageButton
            mBtnGallery = mView.findViewById(R.id.btnOpenGallery) as ImageButton
            mImageView = mView.findViewById(R.id.imageView) as ImageView
            mProgressBar = mView.findViewById(R.id.pgbar) as ProgressBar
            mRetryButton = mView.findViewById(R.id.btnRetry) as ImageButton
            picasso = Picasso.Builder(mView.context)
                    .listener { picasso, uri, exception -> Log.d(TAG, exception.toString()) }
                    .build()
        }

        fun bind(imageUpload: ImageUploadModel, listener: ImageUploadAdapterListener) {
            if (imageUpload!!.image.fileNameWithlocalPath != null) {
                picasso.load(File(imageUpload!!.image.fileNameWithlocalPath)).placeholder(R.drawable.thumbnail_launcher).fit().centerInside().into(mImageView)
                // uploadImage(image)
            } else {
                picasso.load(R.drawable.thumbnail_launcher).placeholder(R.drawable.thumbnail_launcher).fit().centerInside().into(mImageView)
            }

//            mRetryButton = image.retryBtn
            mProgressBar = imageUpload.pgbar

            mBtnCamera.onClick {
                onClickListener.cameraBtnOnClick(mView, adapterPosition)
            }

            mBtnGallery.onClick{
                Log.d(TAG, "gallery clicked")
                onClickListener.galleryBtnOnClick(mView, adapterPosition)
            }

            mRetryButton.onClick{
                Log.d(TAG, "retry clicked")
                onClickListener.retryBtnOnClick(mView, adapterPosition)
            }
        }
    }
}