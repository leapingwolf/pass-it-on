package de.com.pass_it_on.adapters;

//import com.aerialroots.aerialroots.fragmenttest.dummy.DummyContent.DummyItem
//import com.aerialroots.aerialroots.fragmenttest.dummy.DummyContent.DummyItem
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import de.com.pass_it_on.R
import de.com.pass_it_on.fragments.AllItemListFragment
import de.com.pass_it_on.models.ItemUserModel
import java.text.SimpleDateFormat

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class AllItemRecyclerViewAdapter(private val mValues: List<ItemUserModel>,
                                 private val mListener: AllItemListFragment.OnListFragmentInteractionListener?,
                                 private val picasso: Picasso)
: RecyclerView.Adapter<AllItemRecyclerViewAdapter.ViewHolder>(){
    final val TAG : String = "AllItemRecyclerViewAdapter"
    lateinit var context: Context
    //    lateinit var picasso: Picasso
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_allitem, parent, false)
        context = parent.context
        return ViewHolder(view)
    }

    fun getAdapter(): AllItemRecyclerViewAdapter {
        return this
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = mValues[position]
        //TODO: using id for testing only
        if (mValues[position].item!!.offerType != null){
            if (mValues[position].item!!.offerType == 2) { holder.mOfferType.text = "NEED" } else {
                holder.mOfferType.text = "GIVE" }}
        holder.mContentView.text = mValues[position].item!!.title
//        holder.mItemUpdatedDate.text ="edited: " +formatDate(mValues[position].item!!.updated_at.toString())
        holder.mItemInsertedDate.text =formatDate(mValues[position].item!!.inserted_at.toString())
        if(mValues[position].distance != null ) holder.mItemDistance.text = String.format( "%.2f", mValues[position].distance!!.div(1000)) + "km"
        else holder.mItemDistance.text = ""
                //val imageUrl = "https:/6/unsplash.com/photos/x7HJdJZqplo"
        //TODO: remove hardcoded image url, instead use mValues[position].imageUrl
//        var imageUrl = "http://contentservice.mc.reyrey.net/image_v1.0.0/?id=2774412"
    //    var imageUrl: String = "http://contentservice.mc.reyrey.net/image_v1.0.0/?id=2774412"
//        var imageUrl = "http://contentservice.mc.reyrey.net/image_v1.0.0/?id=2774412"
        if(mValues[position]!!.item!!.image!!.count() > 0) {
            picasso.load(mValues[position]!!.item!!.image!![0]!!.url + ".png").placeholder(R.drawable.thumbnail_launcher).fit().into(holder.mImageView)
        }else{
            picasso.load(R.drawable.thumbnail_launcher).placeholder(R.drawable.thumbnail_launcher).fit().into(holder.mImageView)
        }
        //picasso.load(imageUrl).placeholder(R.drawable.placeholder).into(holder.mImageView)
        holder.mView.setOnClickListener {
            mListener?.onListFragmentInteraction(holder.mItem)
        }
    }

    private fun formatDate(postedDate: String?): String? {
        if (postedDate != null) {
//            Log.d(TAG, " raw updated date" + postedDate)
            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val formattedDate = SimpleDateFormat("dd.MM.yyyy hh:mm");
            try {
                val d = dateFormat.parse(postedDate)
                System.out.println("DATE" + d);
                return formattedDate.format(d)
            } catch(e: Exception) {
                System.out.println("Excep" + e.message);
            }
        }
        return null
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mOfferType: TextView
        val mContentView: TextView
        val mImageView: ImageView
//        val mItemUpdatedDate: TextView
        val mItemInsertedDate: TextView
        val mItemDistance: TextView
        lateinit var mItem: ItemUserModel

        init {
            mOfferType = mView.findViewById(R.id.offerType) as TextView
            mContentView = mView.findViewById(R.id.title) as TextView
//            mItemUpdatedDate = mView.findViewById(R.id.itemUpdatedDate) as TextView
            mItemInsertedDate= mView.findViewById(R.id.itemInsertedDate) as TextView
            mItemDistance = mView.findViewById(R.id.itemDistance) as TextView
            mImageView = mView.findViewById(R.id.image) as ImageView
        }

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
