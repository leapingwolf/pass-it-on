package de.com.pass_it_on.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import de.com.pass_it_on.R
import de.com.pass_it_on.models.MessageResponse
import java.text.SimpleDateFormat

/**
 * Created by akshata on 24/08/16.
 */
class MessageRecyclerViewAdapter(private val activityContext: Context, private val currentUserId: Int?, private val mValues: List<MessageResponse?>) : RecyclerView.Adapter<MessageRecyclerViewAdapter.ViewHolder>() {
    final val TAG : String = "MessageRecyclerViewAdapter"
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_message, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = mValues[position]!!
//        holder.mSender.text = mValues[position]!!.sender

        holder.mMessageDate.text = formatDate(mValues[position]!!.message!!.inserted_at.toString())
        if(mValues[position]!!.message!!.sender_id == currentUserId ){
            holder.mMessageBody.text = mValues[position]!!.message!!.body
            holder.mOtherGuy.text = mValues[position]!!.sender + " says:"
        }
        if(mValues[position]!!.message!!.receiver_id == currentUserId ){
            holder.mMessageBody.text = mValues[position]!!.message!!.body
            holder.mOtherGuy.text = mValues[position]!!.sender + " says"
        }
//        picasso.load(imageUrl).placeholder(R.drawable.placeholder).into(holder.mImageView)
//        holder.mView.setOnClickListener {
//            mListener?.onListFragmentInteraction(holder.mItem!!)
//        }
    }

    private fun formatDate(postedDate: String?): String? {
        if (postedDate != null) {
//            Log.d(TAG, " raw updated date" + postedDate)
            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val formattedDate = SimpleDateFormat("dd.MM.yyyy hh:mm");
            try {
                val d = dateFormat.parse(postedDate)
                System.out.println("DATE" + d);
                return formattedDate.format(d)
            } catch(e: Exception) {
                System.out.println("Excep" + e.message);
            }
        }
        return null
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
//        val mSender: TextView
        val mOtherGuy: TextView
        val mMessageBody: TextView
        val mMessageDate: TextView
        lateinit  var mItem: MessageResponse


        init {
//            mMessageInfo = mView.findViewById(R.id.messageInfo) as TextView
//            mSender = mView.findViewById(R.id.sender) as TextView
            mOtherGuy = mView.findViewById(R.id.otherGuy) as TextView
            mMessageBody = mView.findViewById(R.id.body) as TextView
            mMessageDate = mView.findViewById(R.id.insertedDate) as TextView
        }

//        override fun toString(): String {
//            return super.toString() + " '" + mConversationDate.text + "'"
//        }

    }
}