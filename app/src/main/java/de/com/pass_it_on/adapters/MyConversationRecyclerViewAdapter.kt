package de.com.pass_it_on.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import de.com.pass_it_on.R
import de.com.pass_it_on.fragments.ConversationFragment.OnListFragmentInteractionListener
import de.com.pass_it_on.models.ConversationResponse
import java.text.SimpleDateFormat

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class MyConversationRecyclerViewAdapter(private val currentUserId: Int?, private val mValues: List<ConversationResponse>,
                                        private val mListener: OnListFragmentInteractionListener?,
                                        private val picasso: Picasso) : RecyclerView.Adapter<MyConversationRecyclerViewAdapter.ViewHolder>() {
    final val TAG : String = "MyConversationRecyclerViewAdapter"
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_conversation, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = mValues[position]
        holder.mItemTitle.text = mValues[position].itemTitle.toString()
        holder.mConversationDate.text =formatDate(mValues[position].conversation!!.updated_at.toString())
//        holder.mReceiver.text = mValues[position].receiverName!!
        //var imageUrl: String = "http://contentservice.mc.reyrey.net/image_v1.0.0/?id=2774412"
        if(mValues[position]!!.image != null) {
         picasso.load(mValues[position]!!.image!!.url + ".png").placeholder(R.drawable.thumbnail_launcher).fit().into(holder.mImageView)
        }else{
            picasso.load(R.drawable.thumbnail_launcher).placeholder(R.drawable.thumbnail_launcher).fit().into(holder.mImageView)
        }
        holder.mView.setOnClickListener {
            mListener?.onListFragmentInteraction(holder.mItem!!)
        }
        if(mValues[position].itemOfferType != null)
            if(mValues[position].itemOfferType == 2) {   holder.mOfferType.text = "NEED"  }
            else { holder.mOfferType.text = "GIVE" }

        if(mValues[position]!!.sender_id == currentUserId ){
            holder.mItemTitle.text = "Message sent for " +  mValues[position]!!.itemTitle
            holder.mSenderReceiver.text = mValues[position]!!.receiverName
        }
        if(mValues[position]!!.receiver_id == currentUserId ){
            holder.mItemTitle.text = "Message received for " +  mValues[position]!!.itemTitle
            holder.mSenderReceiver.text =  mValues[position]!!.senderName
        }

        holder.mConversationDate.text = formatDate(mValues[position]!!.conversation!!.updated_at.toString())
    }

    private fun formatDate(postedDate: String?): String? {
        if (postedDate != null) {
//            Log.d(TAG, " raw updated date" + postedDate)
            val dateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
            val formattedDate = SimpleDateFormat("dd.MM.yyyy hh:mm")
            try {
                val d = dateFormat.parse(postedDate)
                System.out.println("DATE" + d);
                return formattedDate.format(d)
            } catch(e: Exception) {
                System.out.println("Excep" + e.message);
            }
        }
        return null
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mItemTitle: TextView
        val mConversationDate: TextView
        val mSenderReceiver: TextView
        val mImageView: ImageView
        val mOfferType: TextView

       lateinit  var mItem: ConversationResponse


        init {
            mItemTitle = mView.findViewById(R.id.itemTitle) as TextView
            mOfferType = mView.findViewById(R.id.offerType) as TextView
            mSenderReceiver = mView.findViewById(R.id.messageSenderReceiver) as TextView
            mConversationDate = mView.findViewById(R.id.conversationInsertedDate) as TextView
            mImageView = mView.findViewById(R.id.image) as ImageView
        }

//        override fun toString(): String {
//            return super.toString() + " '" + mConversationDate.text + "'"
//        }

    }
}
