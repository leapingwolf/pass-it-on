package de.com.pass_it_on

import android.content.Context
import android.support.multidex.MultiDex
import android.util.Log
import de.com.pass_it_on.utils.GoogleApiHelper
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

/**
 * Created by akshata on 09/09/16.
 */
class App : android.app.Application() {
    lateinit var googleApiHelperInstance: GoogleApiHelper
        private set

    override fun onCreate() {
        super.onCreate()
        instance = this
        googleApiHelperInstance = GoogleApiHelper(applicationContext)
        Log.d("Application", "app started")

        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-ThinItalic.ttf")
                .setFontAttrId(R.attr.fontPath).build())
    }

    override fun attachBaseContext(context: Context){
        super.attachBaseContext(context)
//       super.attachBaseContext(CalligraphyContextWrapper.wrap(context))
        MultiDex.install(this)
    }
    companion object {
        var instance: App? = null
            private set
        val googleApiHelper: GoogleApiHelper
            get() = instance!!.googleApiHelperInstance
    }
}

